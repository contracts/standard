# 标准协议草案

本目录为当前进行中的合约协议的草案：有任何建议或意见均可联系我们： [联系我们](https://docs.chainmaker.org.cn/quickstart/%E9%95%BF%E5%AE%89%E9%93%BE%E5%9F%BA%E7%A1%80%E7%9F%A5%E8%AF%86%E4%BB%8B%E7%BB%8D.html)



## 第一草案

包含如下合约标准：

CM-CS-221221-Base.md (CMBase-1）长安链基础合约标准协议

CM-CS-221221-Identity (CMID-1）身份认证合约标准协议

CM-CS-221221-NFA（CMNFA-1）数字藏品类合约标准协议

CM-CS-221221-DFA（CMDFA-1）同质化数字资产类合约标准协议

CM-CS-221221-Evidence (CMEVI-1） 区块链存证类合约标准协议

> 已于22年12月21日正式启用。[查看](../living)

## 第二草案

CM-CS-231201-BNS（CMBNS-1）BNS区块链名称服务合约标准协议

CM-CS-231201-DID（CMDID-1）去中心化数字身份合约标准协议