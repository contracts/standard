## CM-CS-231201-BNS（CMBNS-1）BNS区块链名称服务合约标准协议整体介绍

### 背景及目的
长安链区块链名称服务合约标准（BNS）合约标准`CM-CS-231201-BNS`全称`Chainmaker - Contract Standard - BlockChain Name Service - 231201版`简称`CMBNS-1`，旨在为区块链地址提供易于识别的名称，类似于互联网上的域名系统（DNS）。BNS合约允许用户在区块链上注册并拥有域名，并将其解析到相关的区块链地址。BNS合约标准支持多种合约开发语言，如Go、Rust、C++和TinyGo等，适用于各种区块链项目。
BNS合约标准签入了NFA合约标准，支持NFA合约标准中的所有方法，每个注册的一级域名都对应一个NFA，其Category为上一级域名的名称，NFA的TokenID就是完整的域名名称，
在本处不再重复说明每个NFA合约的函数。

### 适用场景
本标准支持Go、Rust、C++和TinyGo等除Solidity外的多种合约开发语言，适用于适用于需要在区块链上实现域名服务的场景，如去中心化应用（DApp）中的地址解析、去中心化身份识别等。修订记录

### 修订记录

#### CMBNS-1

* 修订内容：合约标准第一版

## 协议详情
本协议将BNS合约分为BNS主合约和BNS解析器合约两部分，BNS主合约是必须实现的，BNS解析器合约是可选的，如果不实现则采用BNS主合约作为默认的解析器合约。
用户在解析域名或者反向解析时都是采用BNS主合约的解析方法，如果用户设置了解析器合约，主合约会自动跨合约调用解析器合约的Resolve解析方法。
所有反向解析都通过ReverseBind绑定在BNS主合约中，如果用户设置了解析器合约，解析器合约需要通过跨合约调用BNS主合约的ReverseBind来绑定反向解析。
NFA合约标准中的Approve方法，除了授权给其他账户可以转移Token外，还可以授权给其他账户可以绑定域名，这样其他账户就可以通过BNS合约的Bind方法来绑定域名。
### 基础对象
#### DomainInfo
```go
// DomainInfo 域名信息
type DomainInfo struct {
    // Domain 域名
    Domain string `json:"domain"`
    // ResolveValue 解析地址
    ResolveValue string `json:"resolveValue"`
    // Owner 所有者
    Owner string `json:"owner"`
    // ExpirationTime 过期时间
    ExpirationTime int64 `json:"expirationTime"`
    // Resolver 解析器合约地址
    Resolver string `json:"resolver"`
    // Status 域名状态，正常，过期，禁用
    Status string `json:"status"`
    // Metadata 元数据
    Metadata string `json:"metadata"`
}
```


### 基础方法介绍

#### 合约函数集

| 函数名                  | 函数类型 | 函数说明            | 备注                    |
|----------------------|-----|-----------------|-----------------------|
| Domain               | 查询  | 获取根域名           | 必须                    |
| Register             | 调用  | 注册域名            | 必须，成功则触发Register事件    |
| Renew                | 调用  | 续期域名            | 必须                    |
| GetDomainInfo        | 查询  | 获取域名信息          | 必须                    |
| GetDomainList        | 查询  | 获取域名列表          | 必须                    |
| Bind                 | 调用  | 绑定域名            | 必须，成功则触发Bind事件        |
| Unbind               | 调用  | 解绑域名            | 必须                    |
| Resolve              | 查询  | 解析域名            | 必须                    |
| ReverseBind          | 调用  | 绑定反向解析值和域名的映射   | 必须                    |
| ReverseUnbind        | 调用  | 解绑反向解析值和域名的映射   | 必须                    |
| ReverseResolve       | 查询  | 反向解析地址          | 必须                    |
| SetResolver          | 调用  | 设置域名解析器         | 必须，成功则触发SetResolver事件 |
| ResetResolver        | 调用  | 重置域名解析器         | 必须                    |
| EmitRegisterEvent    | 事件  | 发送注册域名事件        | 必须                    |
| EmitSetResolverEvent | 事件  | 发送设置域名解析器事件     | 必须                    |
| EmitBindEvent        | 事件  | 发送域名绑定事件        | 必须                    |
| EmitReverseBindEvent | 事件  | 发送反向域名绑定事件      | 必须                    |

以下是每个函数的详细介绍：

##### Domain

```go
// Domain 获取根域名
// @return string 返回根域名
Domain() string
```

##### Register

```go
// Register 注册域名
// @param domain 要注册的域名
// @param owner 域名所有者
// @param expirationTime 域名过期时间,time.Unix时间
// @return error 注册失败则返回Status：ERROR，Message具体错误
Register(domain string, owner string, expirationTime int64) error
```

##### RenewDomain

```go
// Renew 续期域名
// @param domain 要续期的域名
// @param expirationTime 域名新的过期时间
// @return error 续期失败则返回Status：ERROR，Message具体错误
Renew(domain string, expirationTime int64) error
```

##### GetDomainInfo

```go
// GetDomainInfo 获取域名信息
// @param domain 要查询的域名
// @return *DomainInfo 返回域名信息结构体
// @return error 查询失败则返回Status：ERROR，Message具体错误
GetDomainInfo(domain string) (*DomainInfo, error)
```

##### GetDomainList

```go
// GetDomainList 获取域名列表
// @param domainSearch 域名搜索关键字
// @param owner 域名所有者
// @param start 列表起始位置，从0开始
// @param count 列表数量，默认0表示返回所有
// @return []*DomainInfo 返回域名信息结构体数组
// @return error 查询失败则返回Status：ERROR，Message具体错误
GetDomainList(domainSearch string, owner string, start int, count int) ([]*DomainInfo, error)
```

##### Bind

```go
// Bind 绑定一级二级或者更子级的域名到指定的值
// @param domain 要绑定的域名
// @param value 要绑定的值（如区块链地址）
// @param resourceType 要绑定的值的类型，如：Address，URL，ContentHash，Text，可以为空
// @return error 绑定失败则返回Status：ERROR，Message具体错误
Bind(domain string, value string, resourceType string) error
```

##### Unbind

```go
// Unbind 解绑域名
// @param domain 要解绑的域名
// @param resourceType 要解绑的值的类型，如：Address，URL，ContentHash，Text，可以为空
// @return error 解绑失败则返回Status：ERROR，Message具体错误
Unbind(domain string, resourceType string) error
```

##### Resolve

```go
// Resolve 解析域名
// @param domain 要解析的域名
// @param resourceType 要解析的值的类型，如：Address，URL，ContentHash，Text，可以为空
// @return string 返回解析结果（如区块链地址）
// @return error 解析失败则返回Status：ERROR，Message具体错误
Resolve(domain string, resourceType string) (string, error)
```

##### ReverseBind

所有其他自定义的Resolver合约如果要实现反向解析，在Bind的时候都必须调用主BNS合约的ReverseBind方法，用于反向注册解析值和域名的映射。
```go
// ReverseRegistrar 反向注册解析值和域名的映射
// @param address 要反向注册的地址
// @param domain 要反向注册的域名
// @param resourceType 要反向注册的值的类型，如：Address，URL，ContentHash，Text，可以为空
// @return error 反向注册失败则返回Status：ERROR，Message具体错误
ReverseBind(address string, domain string, resourceType string) error
```

#### ReverseUnbind

所有其他自定义的Resolver合约如果要实现反向解析，再Unbind的时候都必须调用主BNS合约的ReverseUnbind方法，用于解绑反向解析值和域名的映射。
```go
// ReverseUnbind 反向解绑解析值和域名的映射
// @param address 要反向解绑的地址
// @param resourceType 要反向解绑的值的类型，如：Address，URL，ContentHash，Text，可以为空
// @return error 反向解绑失败则返回Status：ERROR，Message具体错误
ReverseUnbind(address string, resourceType string) error
```


##### ReverseResolve

```go
// ReverseResolve 反向解析地址
// @param address 要反向解析的地址
// @param resourceType 要反向解析的值的类型，如：Address，URL，ContentHash，Text，可以为空
// @return string 返回解析结果（如域名）
// @return error 解析失败则返回Status：ERROR，Message具体错误
ReverseResolve(address string, resourceType string) (string, error)
```

##### SetResolver

```go
// SetResolver 设置域名解析器的合约名称或者地址，通过这个合约来解析其下的子域名
// @param domain 要设置解析器的域名
// @param resolver 解析器合约地址
// @return error 设置失败则返回Status：ERROR，Message具体错误
SetResolver(domain string, resolver string) error
```

##### ResetResolver

```go
// ResetResolver 重置域名解析器，重置后将采用系统默认的域名解析器
// @param domain 要重置解析器的域名
// @return error 重置失败则返回Status：ERROR，Message具体错误
ResetResolver(domain string) error
```

##### EmitRegisterEvent

```go
// EmitRegisterEvent 发送注册域名事件
// @param domain 注册的域名
// @param owner 域名所有者
// @param expirationTime 域名过期时间
EmitRegisterEvent(domain string, owner string, expirationTime int64)
```

##### EmitSetResolverEvent

```go
// EmitSetResolverEvent 发送设置域名解析器事件
// @param domain 设置解析器的域名
// @param resolver 解析器合约地址，如果为空则表示重置解析器
EmitSetResolverEvent(domain string, resolver string)
```

##### EmitBindEvent

```go
// EmitBindEvent 发送域名绑定事件
// @param domain 绑定的域名
// @param address 绑定的地址，如果为空则表示解绑
// @param resourceType 绑定的值的类型，如：Address，URL，ContentHash，Text，可以为空
EmitBindEvent(domain string, address string, resourceType string)
```
##### EmitReverseBindEvent

```go
// EmitReverseBindEvent 发送反向域名绑定事件
// @param address 绑定的地址
// @param domain 绑定的域名，如果为空则表示解绑
// @param resourceType 绑定的值的类型，如：Address，URL，ContentHash，Text，可以为空
EmitReverseBindEvent(address string, domain string, resourceType string)
```

以上是BNS合约标准中基础方法每个函数的详细介绍。通过实现这些接口，可以方便地在区块链上实现域名服务，为区块链项目提供更友好的用户体验。

### 解析器方法介绍
在上述的基础方法中，有以下方法是解析合约必须实现的方法，解析合约可以是系统默认的解析合约，也可以是自定义的解析合约。
需要注意，如果需要反向解析，自定义解析器合约需要调用主BNS合约的ReverseBind和ReverseUnbind方法，用于反向注册解析值和域名的映射。

| 函数名                  | 函数类型 | 函数说明         | 备注                 |
|----------------------|-----|--------------|--------------------|
| Bind                 | 调用  | 绑定域名         | 必须，成功则触发Bind事件 |
| Unbind               | 调用  | 解绑域名         | 必须                 |
| Resolve              | 查询  | 解析域名         | 必须                 |
| ReverseResolve       | 查询  | 反向解析地址       | 必须                 |
| EmitSetResolverEvent | 事件  | 发送设置域名解析器事件  | 必须              |
| EmitBindEvent        | 事件  | 发送域名绑定事件     | 必须              |


### 可拓展方法介绍

#### 合约函数集
| 函数名               | 函数类型 | 函数说明              | 备注              |
| ---                 | ---     |-------------------|-----------------|
| AddBlackList | 调用   | 添加黑名单          | 可选，成功则触发AddBlackList事件 |
| DeleteBlackList| 调用 | 删除黑名单          | 可选，成功则触发DeleteBlackList事件 |
| GetBlackList | 查询   | 获取黑名单          | 可选                 |
| EmitAddBlackListEvent| 事件    | 发送添加黑名单事件       | 可选              |
| EmitDeleteBlackListEvent| 事件 | 发送删除黑名单事件       | 可选              |



##### AddBlackList

```go
// AddBlackList 添加黑名单
// @param domains 要添加到黑名单的域名列表
// @return error 添加失败则返回Status：ERROR，Message具体错误
AddBlackList(domains []string) error
```

##### DeleteBlackList

```go
// DeleteBlackList 删除黑名单
// @param domains 要从黑名单中删除的域名列表
// @return error 删除失败则返回Status：ERROR，Message具体错误
DeleteBlackList(domains []string) error
```

##### GetBlackList

```go
// GetBlackList 获取黑名单
// @param domainSearch 黑名单域名搜索关键字
//
// @param start 列表起始位置
// @param count 列表数量
// @return []string 返回黑名单域名列表
// @return error 查询失败则返回Status：ERROR，Message具体错误
GetBlackList(domainSearch string, start int, count int) ([]string, error)
```


##### EmitAddBlackListEvent

```go
// EmitAddBlackListEvent 发送添加黑名单事件
// @param domains 添加到黑名单的域名列表
EmitAddBlackListEvent(domains []string)
```

##### EmitDeleteBlackListEvent

```go
// EmitDeleteBlackListEvent 发送删除黑名单事件
// @param domains 从黑名单中删除的域名列表
EmitDeleteBlackListEvent(domains []string)
```

## 事件

在BNS合约标准中，有一些操作会触发事件。事件可以帮助外部监听器（如钱包、区块链浏览器等）追踪合约内发生的重要状态变化。以下是BNS合约标准中涉及的事件及其数据：

### Topic: Register

注册域名事件，当成功注册一个域名时触发。

数据：
* domain string 域名
* owner string 域名所有者
* expirationTime int64 域名过期时间,time.Unix时间

### Topic: SetResolver

设置域名解析器事件，当成功为一个域名设置解析器时触发。

数据：
* domain string 域名
* resolver string 解析器合约地址

### Topic: Bind

域名绑定事件，当成功为一个域名绑定地址时触发。

数据：
* domain string 域名
* address string 绑定的地址
* resourceType string 绑定的值的类型，如：Address，URL，ContentHash，Text，可以为空

### Topic: ReverseBind

反向域名绑定事件，当成功为一个地址绑定域名时触发。

数据：
* address string 绑定的地址
* domain string 绑定的域名
* resourceType string 绑定的值的类型，如：Address，URL，ContentHash，Text，可以为空

### Topic: AddBlackList

添加黑名单事件，当成功将一个或多个域名添加到黑名单时触发。

数据：
* domains []string 添加到黑名单的域名列表

### Topic: DeleteBlackList

删除黑名单事件，当成功将一个或多个域名从黑名单中删除时触发。

数据：
* domains []string 从黑名单中删除的域名列表

通过监听这些事件，外部应用可以实时了解域名注册、绑定、解析器设置以及黑名单变动等信息，从而为用户提供更好的服务。


## 示例合约
[BNS-go](https://git.chainmaker.org.cn/contracts/contracts-go/-/tree/master/standard-bns)

### 结论
以上是BNS合约标准的详细介绍。通过实现这些接口，可以方便地在区块链上实现域名服务，为区块链项目提供更友好的用户体验。