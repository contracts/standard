## CM-CS-221221-Identity (CMID-1）身份认证合约标准协议整体介绍

### 背景及目的
长安链身份认证合约标准`CM-CS-221221-Identity`全称`Chainmaker - Contract Standard - Identity-20221221版`，简称`CMID-1`，
是官方推出的基于长安链的身份认证合约的撰写和使用规范。
方便大家在编写相关合约时能够做到有章可循，长安链的各生态工具也会基于此标准对相应的合约进行适配。
在国内，区块链上链信息可监管，实名制几乎已成共识。为了将身份信息标准化，方便合约开发者获取当前发起人的身份信息，故推出该身份认证合约。

### 适用场景

在某些需要获取发交易的人是否实名认证时。比如：在发行CMDNA、CMDFA、CMEVI时，要求该地址必须为企业实名认证。则可直接调用该合约方法获取认证类型编号，再对比认证类型编号是否合法。 

## 协议详情


### 基础方法介绍

> 注：若入参为结构体：则为json格式字符串
>
> 若返回为结构体：则也是json格式字符串返回

#### 合约函数集

| 函数名      | 函数类型 | 函数说明                     | 备注             |
| ----------- | -------- | ---------------------------- | ---------------- |
| Identities  | 查询     | 获取该合约支持的所有认证类型 | 任何人可调用     |
| SetIdentity | 调用     | 设置认证类型编号             | 一般管理员可调用 |
| IdentityOf  | 查询     | 获取认证信息                 | 任何人可调用     |
| LevelOf     | 查询     | 获取认证编号                 | 任何人可调用     |

**结构体**

```go
// IdentityMeta 认证类型基础信息
type IdentityMeta struct {
	// Level 认证类型编号
	Level int `json:"level"`
	// Description 认证类型描述
	Description string `json:"description"`
}
// Identity 认证类型入参
type Identity struct {
	// Address 公钥地址
	Address string `json:"address"`
	// PkPem 公钥详情
	PkPem string `json:"pkPem"`
	// Level 认证类型编号
	Level int `json:"level"`
	// Metadata 其他，json格式字符串，可包含地址类型，上链人身份、组织信息，上链可信时间，上链批次等等
	Metadata string `json:"metadata"`
}

// Metadata 可选信息建议字段，若包含以下相关信息，建议采用以下字段
type Metadata struct {
	// AddressType 地址类型：0-chainmaker, 1-zxl, 2-ethereum，长安链默认是2
	AddressType string `json:"addressType"`
	// OrgId 组织ID
	OrgId string `json:"orgId"`
	// Role 上链人身份角色
	Role string `json:"role"`
	// Timestamp 可信存证时间
	Timestamp string `json:"timestamp"`
	// ProveTimestamp 可信存证时间证明
	ProveTimestamp string `json:"proveTimestamp"`
	// BatchId 批次ID
	BatchId string `json:"batchId"`
	// 其他自定义扩展字段
	// ...
}
```

##### Identities

```go
// Identities 获取该合约支持的所有认证类型
// @return metas, 所有的认证类型编号和认证类型描述
Identities() (metas []IdentityMeta)
```

**SetIdentity**

```go
// SetIdentity 为地址设置认证类型，管理员可调用
// @param address 必填，公钥/证书的地址。一个地址仅能绑定一个公钥和认证类型编号，重复输入则覆盖。
// @param pkPem 选填,pem格式公钥，可用于验签
// @param level 必填,认证类型编号
// @param metadata 选填,其他信息，json格式字符串，比如：地址类型，上链人身份、组织信息，上链可信时间，上链批次等等
// @return error 返回错误信息
// @event topic: setIdentity(address, level, pkPem)
SetIdentity(address, pkPem string, level int, metadata string) error
```

##### IdentityOf

```go
// IdentityOf 获取认证信息
// @param address 地址
// @return int 返回当前认证类型编号
// @return identity 认证信息
// @return err 返回错误信息
IdentityOf(address string) (identity Identity, err error)
```

**LevelOf**

```go
// LevelOf 获取认证编号
// @param address 地址
// @return level 返回当前认证类型编号
// @return err 返回错误信息
LevelOf(address string) (level int, err error)
```

### 可拓展方法介绍

| 函数名            | 函数类型 | 函数说明                     | 备注             |
| ----------------- | -------- | ---------------------------- | ---------------- |
| SetIdentityBatch  | 调用     | 批量设置认证类型编号         | 一般管理员可调用 |
| AlterAdminAddress | 调用     | 设置当前合约管理员           | 管理员可调用     |
| PkPemOf           | 查询     | 获取pem编码公钥              | 任何人可调用     |

#### 合约函数集

##### SetIdentityBatch

```go
// SetIdentityBatch 设置多个认证类型，管理员可调用
// @param identities, 入参json格式字符串
// @event topic: SetIdentity(address, level, pkPem)
SetIdentityBatch(identities []Identity) error
```

##### AlterAdminAddress

设置当前合约管理员，一般而言调用SetIdentity、SetIdentityBatch时，需要验证是否是管理人员

```go
// AlterAdminAddress 修改管理员，管理员可调用
// @param adminAddresses 管理员地址，可为空，默认为创建人地址。入参为以逗号分隔的地址字符串"addr1,addr2"
// @return error 返回错误信息
// @event topic: AlterAdminAddress（adminAddresses）
AlterAdminAddress(adminAddresses []string) error
```

##### PkPemOf

```go
// PkPemOf 获取公钥
// @param address 地址
// @return string 返回当前地址绑定的公钥
// @return error 返回错误信息
PkPemOf(address string) (string, error)
```

### 事件

#### SetIdentity

- address
- level
- pkPem

#### AlterAdminAddress

- adminAddresses


## 示例合约
[CMID-go](https://git.chainmaker.org.cn/contracts/contracts-go/-/tree/master/standard-identity)