## CM-CS-231201-Evidence (CMEVI-2） 区块链存证类合约标准协议整体介绍

### 背景及目的
长安链存证合约标准`CM-CS-231201-Evidence`全称`Chainmaker - Contract Standard - Evidence-231201版`，简称`CMEVI-2`，
区块链存证存证是区块链上最常见的应用之一，CMEVI-2是官方推出的基于长安链的存证类合约的撰写和使用规范。方便大家在编写相关合约时能够做到有章可循，长安链的各生态工具也会基于此标准对相应的合约进行适配。
历史版本CMEVI-1：[CMEVI-1](../history/CM-CS-221221-Evidence.md)

### 适用场景
该功能常用于区块链+司法存证业务，在事件发生时，先将产生的电子信息存证到区块链上进行证据固定；当需要提交诉讼时，由原告出示电子信息原件，再通过存证核验比对证据是否与上链固定时一致，检验证据的有效性。


###  修订记录
#### CMEVI-1
* 修订内容：存证合约标准第一版
#### CMEVI-2
* 修订内容：新增存证事件Evidence

## 协议详情

### 综述
### 基础方法介绍
> 注：若入参为结构体：则为json格式字符串
>
> 若返回为结构体：则也是json格式字符串返回


#### 合约函数集

| 函数名                | 函数类型 | 函数说明    | 备注 |
|--------------------|------|---------|----|
| Evidence           | 调用   | 存证      | 必选 |
| FindByHash         | 查询   | 根据哈希查找  | 必选 |
| FindById           | 查询   | 根据ID查找  | 必选 |
| ExistsOfHash       | 查询   | 哈希是否存在  | 必选 |
| ExistsOfId         | 查询   | ID是否存在  | 必选 |
| EmitEvidenceEvent  | 事件   | 触发存证事件 | 必选 |

**结构体**

```go
// Evidence 存证结构体
type Evidence struct {
	// Id 业务流水号
	Id string `json:"id"`
	// Hash 哈希值
	Hash string `json:"hash"`
	// TxId 存证时交易ID
	TxId string `json:"txId"`
	// BlockHeight 存证时区块高度
	BlockHeight int `json:"blockHeight"`
	// Timestamp 存证时区块时间
	Timestamp string `json:"timestamp"`
	// Metadata 可选，其他信息；具体参考下方 Metadata 对象。
	Metadata string `json:"metadata"`
}

// Metadata 可选信息建议字段，若包含以下相关信息存证，请采用以下字段
type Metadata struct {
	// HashType 哈希的类型，文字、文件、视频、音频等
	HashType string `json:"hashType"`
	// HashAlgorithm 哈希算法，sha256、sm3等
	HashAlgorithm string `json:"hashAlgorithm"`
	// Username 存证人，用于标注存证的身份
	Username string `json:"username"`
	// Timestamp 可信存证时间
	Timestamp string `json:"timestamp"`
	// ProveTimestamp 可信存证时间证明
	ProveTimestamp string `json:"proveTimestamp"`
	// 存证内容
	Content string `json:"content"`
	// 其他自定义扩展字段
	// ...
}
```

##### Evidence

```go
// Evidence 存证
// @param id 必填，流水号
// @param hash 必填，上链哈希值
// @param metadata 可选，其他信息；比如：哈希的类型（文字，文件）、文字描述的json格式字符串，具体参考下方 Metadata 对象。
// @return error 返回错误信息
Evidence(id string, hash string, metadata string) error
```
##### ExistsOfHash

```go
// ExistsOfHash 哈希是否存在
// @param hash 必填，上链的哈希值
// @return exist 存在：true，"true"；不存在：false，"false"
// @return err 错误信息
ExistsOfHash(hash string) (exist bool, err error)
```

##### ExistsOfId

```go
// ExistsOfId 哈希是否存在
// @param id 必填，上链的ID值
// @return exist 存在：true，"true"；不存在：false，"false"
// @return err 错误信息
ExistsOfId(id string) (exist bool, err error)
```

**FindByHash**

```go
// FindByHash 根据哈希查找
// @param hash 必填，上链哈希值
// @return evidence 上链时传入的evidence信息
// @return err 返回错误信息
FindByHash(hash string) (evidence *Evidence, err error)
```

**FindById**

```go
// FindById 根据id查找
// @param id 必填，流水号
// @return evidence 上链时传入的evidence信息
// @return err 返回错误信息
FindById(id string) (evidence *Evidence, err error)
```


**EmitEvidenceEvent**

```go
// EmitEvidenceEvent 触发存证事件
// @param id 必填，流水号
// @param hash 必填，上链哈希值
// @param metadata 可选，其他信息；比如：哈希的类型（文字，文件）、文字描述的json格式字符串，具体参考下方 Metadata 对象。
// @return error 返回错误信息
EmitEvidenceEvent(id string, hash string, metadata string) error
```

### 可拓展方法介绍
#### 合约函数集

| 函数名        | 函数类型 | 函数说明                              | 备注                                                    |
|------------| ---     | ---                                 |    ---                                                    |
| EvidenceBatch | 调用  | 批量保存哈希    |                                 |
| UpdateEvidence | 调用 | 根据ID更新存证哈希和metadata | |
| FindHisById | 查询 | 根据ID流水号查找存证历史 | |

##### EvidenceBatch

```go
// EvidenceBatch 批量存证
// @param evidences 必填，存证信息
// @return error 返回错误信息
EvidenceBatch(evidences []Evidence) error
```

**UpdateEvidence**

```go
// UpdateEvidence 根据ID更新存证哈希和metadata
// @param id 必填，已经在链上存证的流水号。 如果是新流水号返回错误信息不存在
// @param hash 必填，上链哈希值。必须与链上已经存储的hash不同
// @param metadata 可选，其他信息；具体参考下方 Metadata 对象。
// @return error 返回错误信息
// @desc 该方法由长安链社区志愿者@sunhuiyuan提供建议，感谢参与
UpdateEvidence(id string, hash string, metadata string) error
```

**FindHisById**

```go
// FindHisById 根据ID流水号查找存证历史(可以使用go合约接口：sdk.Instance.NewHistoryKvIterForKey或NewIterator实现)
// @param id 必填，流水号
// @return string 上链时传入的evidence信息的各个版本JSON数组对象。如果之前上链没有调用过updateEvidence、效果等同于findById，数组大小为1；
//                如果之前上链调用过updateEvidence，则结果数组长度>1。
// @return error 返回错误信息
// @desc 该方法由长安链社区志愿者@sunhuiyuan提供建议，感谢参与
FindHisById(id string) (evidence Evidence, err error)
```

### 事件
存证事件
#### Topic：Evidence
data:
1. id string
2. hash string
3. metadata string



## 参考

http://www.cesi.cn/202207/8542.html

## 示例合约
[CMEVI-go](https://git.chainmaker.org.cn/contracts/contracts-go/-/tree/master/standard-evidence)