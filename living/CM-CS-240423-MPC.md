## CM-CS-240423-MPC(CMMPC-1）长安链多方计算合约标准协议整体介绍

### 背景及目的
长安链多方计算合约标准协议`CM-CS-240423-MPC.md`全称`Chainmaker - Contract Standard - Multiparty Computing-240423版`，简称`CMMPC-1`，
是官方推出的基于长安链的基础合约的撰写和使用规范。方便用户对接隐私计算业务使用。

### 适用场景

本标准是长安链Go语言合约的MPC标准，适用于各类多方计算场景

### 修订记录


#### CMMPC-1

* 修订内容：合约标准第一版

## 协议详情

### 名词解释

* Job：指的是一项工作或任务
* JobInstance：指的是一项工作或任务的示例，每个实例都有自己的属性和状态，如标识符、状态（等待执行、正在执行、已完成等）等。
* Task：指job中需要执行的单个工作单元或操作。它可以是一个功能模块、一个子程序，或者是需要执行的特定操作。
* Service：指常驻的服务。
* ComputingResource：指可以分配和利用的计算资源。
* ComputingModel：指的是描述计算过程和计算机系统行为的抽象模型。

##### 内部存储结构

**Task**
```text
key: "TASK{jobInstanceId}{taskId}" value: EncTask
key: "TASK_STS{jobInstanceId}{taskId}" value: TaskState
key: "TASK_OUT_D{jobInstanceId}{taskId}{outputDataName}" value: Output
key: "TASK_PTY_STS{jobInstanceId}{taskId}{partyId}" value: TaskPartyState
key: "TASK_PTYS{jobInstanceId}{taskId}" value: []string
```


**Service**
```text
key: "SVC_SUB{jobInstanceId}{serviceId}" value: EncService
key: "SVC_STS{jobInstanceId}{serviceId}" value: ServiceState
```

**JobInstance**
```text
key: "JobInstance_SUB{jobInstanceId}" value: EncJobInstance
key: "JobInstance_PTY_STS{partyId}_{jobInstanceState}+{jobInstanceId}" value: jobInstanceId
key: "JobInstance_STS{jobInstanceId}" value: jobInstanceState
key: "JobInstance_SVCS{jobInstanceId}" value: []string
key: "JobInstance_JOB_TASKS{jobInstanceId}" value: []string
key: "JobInstance_PTY{jobInstanceId}" field: partyId value: jobInstanceState
```

**Job**
```text
key: "Job_STAT{jobId}" value: jobStatus
key: "Job_APPROVE{jobId}" value: JobApproveInfo
key: "Job{jobId}" value: EncJob
```

##### 重要结构体


**Task**
```go
type Task struct {
	JobTask  // job的task结构
	// JobInstance Id
	JobInstanceId string `json:"jobInstanceId"` // id
	// Task状态 WAITING/INIT/SETUP/READY/RUNNING/CANCELED/FAILED/SUCCESS
	Status state.TaskState `json:"status"` // task的状态
}

// job的task结构
type JobTask struct {
	TaskName   string    `json:"taskName"` //名称
	TaskLabel  string    `json:"taskLabel"` //标签
	TaskId     string    `json:"taskId"`  //id
	Version    string    `json:"version"`  // 版本
	Module     *Module   `json:"module"`  //模型
	Input      *Input    `json:"input"`   // 任务输入
	OutputList []*Output `json:"outputList"` //任务输出
	PartyList  []*Party  `json:"partyList"`  //参与方列表
	// 变更时间戳
	UpdateTime string `json:"updateTime"`
	// 创建日期时间戳
	CreateTime string `json:"createTime"`
}

// 参与方
type Party struct {
	PartyId    string               `json:"partyId"` // 参与方id
	PartyName  string               `json:"partyName"` //参与方名称
	ServerInfo *ServerInfo          `json:"serverInfo"` //参与方的网络信息
	PubKey     string               `json:"pubKey"` // 平台公钥
}

type ServerInfo struct {
	IP        string `json:"ip"`  // ip地址
	Port      string `json:"port"` //端口
}

// 任务输入结构
type Input struct {
	TaskId          string         `json:"taskId"` // 任务id
	InputDetailList []*InputDetail `json:"inputDataDetailList"` // 输入详情
}

// 任务输入详情
type InputDetail struct {
	DataName     string `json:"dataName"` // 数据名称
	DataId       string `json:"dataId"`  // 数据id
	DomainId     string `json:"domainId"`  //输入数据的所属方id
	DomainName   string `json:"domainName"`  //输入数据的所属方名称
	TaskSrc      string `json:"taskSrc"`  //输入数据的任务来源
	DatabaseName string `json:"databaseName"` //数据的数据库名称
	TableName    string `json:"tableName"`   // 数据的表名
	ColumnName   string `json:"columnName"`  //数据的列名
	Type         string `json:"type"`      // 数据的字段类型
	Length       int    `json:"length"`   // 数据的字段长度
	Comments     string `json:"comments"`  //数据的备注
	AssetName    string `json:"assetName"`  //数据来源的数据资产英文名
	Params       string `json:"params"`    // 数据的自定义参数
}

//任务输出
type Output struct {
	DomainId      string `json:"domainId"` // 输出数据的所属方id
	DomainName    string `json:"domainName"` // 输出数据的所属方名称
	ColumnName    string `json:"columnName"` // 输出数据的列名
	Type          string `json:"type"`    // 输出数据的类型
	Length        int    `json:"length"`  // 输出数据的字段长度
	IsFinalResult bool   `json:"isFinalResult"` //任务是否是最终任务
	DataId        string `json:"dataId"`     // 输出数据的数据id
	DataName      string `json:"dataName"`  // 输出数据的数据名称
	PubKeyName    string `json:"pubKeyName"`  // 输出数据的公钥名称
	PubKey        string `json:"pubKey"` // 输出数据的公钥
}

// 模型参数
type ModuleParam struct {
	Key     string `json:"key"` // 参数的key
	Value   string `json:"value"` // 参数的value
	PartyId string `json:"partyId"` // // 参数所属的参与方
}

// 模型
type Module struct {
	ModuleName string         `json:"moduleName"` // 模型名称
	ParamList  []*ModuleParam `json:"paramList"`  // 模型参数列表
}

```

**service**
```go
// 常驻服务
type Service struct {
	JobService    // job的常驻服务元数据
	JobInstanceId string             `json:"jobInstanceId"`
	Status        state.ServiceState `json:"status"` //service的状态
}

// job的常驻服务元数据
type JobService struct {
	ServiceId               string                 `json:"serviceId"` //id
	Version                 string                 `json:"version"`   // 版本
	PartyId                 string                 `json:"partyId"`  // 参与方id
	PartyName               string                 `json:"partyName"`  // 参与方名称
	ServiceClass            string                 `json:"serviceClass"`  // 常驻服务类型
	ServiceName             string                 `json:"serviceName"`   // 常驻服务名称
	ServiceLabel            string                 `json:"serviceLabel"`  // 常驻服务标签
	ServiceDescription      string                 `json:"serviceDescription"`  // 常驻服务描述
	ExposeEndpointList      []*ExposeEndpoint      `json:"exposeEndpointList"`  // 服务暴露的节点信息列表
	ReferExposeEndpointList []*ReferExposeEndpoint `json:"referExposeEndpointList"`  // 服务引用的节点信息列表
	CreateTime              string                 `json:"createTime"`
	UpdateTime              string                 `json:"updateTime"`
}

// 服务暴露的节点信息
type ExposeEndpoint struct {
	PartyId           string   `json:"partyId"`  // 参与方id
	PartyName         string   `json:"partyName"`  // 参与方名称
	ServiceClass      string   `json:"serviceClass"`  // 常驻服务类型
	Name              string   `json:"name"`         // 节点名称
	TLSEnabled        bool     `json:"tlsEnabled"`  // 是否使用加密通信
	Description       string   `json:"description"` // 暴露服务方法描述
	ServiceCa         string   `json:"serviceCa"`   // TLS证书
	ServiceCert       string   `json:"serviceCert"` // TLS证书
	ServiceKey        string   `json:"serviceKey"`  // TLS
	ServiceCertName   string   `json:"serviceCertName"`  // TLS证书名称
	ApiClientCa       string   `json:"apiClientCa"`     
	ApiClientCert     string   `json:"apiClientCert"`
	ApiClientKey      string   `json:"apiClientKey"`
	ApiClientCertName string   `json:"apiClientCertName"`
	ApiServerCa       string   `json:"apiServerCa"`
	ApiServerCert     string   `json:"apiServerCert"`
	ApiServerKey      string   `json:"apiServerKey"`
	ApiServerCertName string   `json:"apiServerCertName"`
	Protocol          string   `json:"protocol"` // 通信协议类型 HTTP GRPC
	Address           string   `json:"address"`  // hostname:port形式
	Path              string   `json:"path"`     // 仅当protocol:HTTP有效
	Method            string   `json:"method"`   // 仅当protocol:HTTP有效
	ValueList         []*Value `json:"valueList"`  // 节点的参数列表
}


// 暴露节点的参数
type Value struct {
	Key   string `json:"key"` 
	Value string `json:"value"`
}

 // 服务引用的节点信息
type ReferExposeEndpoint struct {
	Id                string        `json:"id"` 
	Name              string        `json:"name"` // 名称
	ReferServiceId    string        `json:"referServiceId"` // 来源的服务id
	ReferEndpointName string        `json:"referEndpointName"` // 来源的节点名称
	ServiceCa         string        `json:"serviceCa"`  // 证书
	ServiceCert       string        `json:"serviceCert"`
	ServiceCertName   string        `json:"serviceCertName"` // 证书名称
	Protocol          string        `json:"protocol"`    // 协议
	Address           string        `json:"address,omitempty"` // 地址
	ReferValueList    []*ReferValue `json:"referValueList"` // 参数列表
}

type ReferValue struct {
	Key            string `json:"key"`
	Value          string `json:"value"`
	ReferServiceId string `json:"referServiceId"` // 引用的服务id
}
```

**Job**
```go
type Job struct {
	JobId string `json:"jobId"` // 明文展示
	//不可为空，计算任务名称
	JobName string `json:"jobName"`
	//不可为空，计算模型
	Description string `json:"description"`
	//不可为空，创建时间
	CreateTime string `json:"createTime"`
	//不可为空，创建者DId
	CreateUserId string `json:"createUserId"`
	//不可为空，创建者组织 name
	CreatePartyName string          `json:"createPartyName"`
	CreatePartyId   string          `json:"createPartyId"`
	ModelType       state.ModelType `json:"modelType"`
	ImplType        state.ImplType  `json:"implType"`
	//不可为空，计算任务描述
	ProcessType          state.ProcessType `json:"processType"` // 处理类型
	PqlText              string            `json:"pqlText"`    // pql
	TaskList             []*JobTask        `json:"taskList"`   // 任务列表
	ServiceList          []*JobService     `json:"serviceList"`  //常驻服务列表
	ResultReceiverList   []*ResultReceiver `json:"resultReceiverList"` //结果接收方列表
	ProcessTimes         int32             `json:"processTimes"`   // 任务处理次数
	ProcessExecutedTimes int32             `json:"processExecutedTimes"` // 任务已经处理的次数
	ProcessEndTime       string            `json:"processEndTime"`    // 任务结束时间
	ProcessInterval      int8              `json:"processInterval"`   // 任务
	ProcessUnit          state.ProcessUnit `json:"processUnit"`
	Status               state.JobStatus   `json:"status"`
	//Status               JobInstanceStatus         `json:"status"`
	AssetDetailList  []*AssetDetail    `json:"assetDetailList"`
	PartyList        []*Party          `json:"partyList"`
	JobTriggerType   int               `json:"triggerType"`      //jobInstance执行触发方式 0-自动触发，1-手动触发
	JobTriggerEnable bool              `json:"jobTriggerEnable"` // JobInstanceTriggerType=1时，是否已触发任务是否已触发任务
	GatewayEnable    bool              `json:"gatewayEnable"`    // GateWayEnable 是否开启网关通讯
	GatewayInfoList  []*GatewayInfo    `json:"gatewayInfoList"`
	IsFinish         bool              `json: "isFinish"`
	IsPql            bool              `json:"isPql"`
	VisibleType      state.VisibleType `json:"visibleType"` // 可见范围： 0 不公开，1部分公开，2全部公开
	Dag              *Dag              `json:"dag"`
	Topology         *Topology         `json:"topology"`
}

// 结果接收方
type ResultReceiver struct {
	Id          string `json:"Id"`  // 接收方id
	PartyId     string `json:"partyId"`  // 参与方id
	PartyName   string `json:"partyName"`  // 参与方名称
	IsEncrypted bool   `json:"isEncrypted"`  // 是否加密
	PubKeyName  string `json:"pubKeyName"`   //公钥名称
	PubKey      string `json:"pubKey"`       // 公钥
}
```

**EncJobInstance**
```go
// JobInstancePrivate 任务调度私有信息
type JobInstancePrivate struct {
	// 请求数据原文
	RequestData string `json:"requestData"`
	// 根据原文生成的task的DAG
	TasksDAG string `json:"tasksDAG"`
}

// EncJobInstance 任务调度数据结构
type EncJobInstance struct {
	JobInstance
	Private *JobInstancePrivate `json:"private"`
	Common  *Common             `json:"common"`
}

type JobInstance struct {
	// job Id
	JobId string `json:"jobId"`
	// JobInstanceId
	JobInstanceId string `json:"jobInstanceId"`
	// JobInstance 名称
	JobInstanceName string `json:"jobInstanceName"`
	//  JobInstance类型 FQ(联邦查询)、FL(联邦学习)
	JobInstanceType     state.ModelType `json:"jobInstanceType"`
	JobInstanceImplType state.ImplType  `json:"jobInstanceImplType"`
	// Job状态  WAITING/APPROVED/READY/RUNNING/CANCELED/FAILED/SUCCESS
	Status state.JobInstanceState `json:"status"`
	// Job提交者Id
	Submitter string `json:"submitter"`
	// 状态变更时间戳
	UpdateTime string `json:"updateTime"`
	// 创建日期时间戳
	CreateTime string `json:"createTime"`
	// 请求数据原文
	RequestData string `json:"requestData"`
	// 根据原文生成的task的DAG
	TasksDAG string `json:"tasksDAG"`
	// 参与方Id数组
	PartyList []string `json:"partyList"`
	// 失败原因
	FailedReason string `json:"failedReason"`
	// 是否开启网关通讯
	GatewayEnable bool      `json:"gatewayEnable"`
	Dag           *Dag      `json:"dag"`
	Topology      *Topology `json:"topology"`
	comm.ChainTxInfo
}
```

**EncJob**
```go
// EncJob 任务数据结构
type EncJob struct {
	Job
	EncJobTaskList     *EncJobTaskList     `json:"encJobTaskList"`     // JobTask 加密信息
	EncJobServiceList  *EncJobServiceList  `json:"encJobServiceList"`  // JobService 加密信息
}

type JobPrivate struct {
	PqlText            string            `json:"pqlText"`
	TaskList           []*JobTask        `json:"taskList"`
	ServiceList        []*JobService     `json:"serviceList"`
	ResultReceiverList []*ResultReceiver `json:"resultReceiverList"`
	AssetDetailList    []*AssetDetail    `json:"assetDetailList"`
	//PartyList          []*Party          `json:"partyList"`
}


// EncJobTaskList
// JobTaskPrivate 任务调度私有信息
type JobTaskPrivate struct {
	Module     *Module   `json:"module"` 
	Input      *Input    `json:"input"`
	OutputList []*Output `json:"outputList"`
	PartyList  []*Party  `json:"partyList"`
}

// EncJobTask 任务调度数据结构
type EncJobTask struct {
	JobTask
	Private *JobTaskPrivate `json:"private"`
	Common  *Common         `json:"common"`
}

type EncJobTaskList struct {
	TaskList []*EncJobTask `json:"taskList"`
}

// 常驻服务调度私有信息
type JobServicePrivate struct {
	ExposeEndpointList      []*ExposeEndpoint      `json:"exposeEndpointList"`
	ReferExposeEndpointList []*ReferExposeEndpoint `json:"referExposeEndpointList"`
}

type EncJobService struct {
	JobService
	Private *JobServicePrivate `json:"private"`
	Common  *Common            `json:"common"`
}

type EncJobServiceList struct {
	ServiceList []*EncJobService `json:"serviceList"`
}


// JobReceiverPrivate  结果接收方私有信息
type JobReceiverPrivate struct {
	PubKeyName string `json:"pubKeyName"`
	PubKey     string `json:"pubKey"`
}

type EncJobReceiver struct {
	ResultReceiver
	Private *JobReceiverPrivate `json:"private"`
	Common  *Common             `json:"common"`
}

type EncResultReceivers struct {
	ResultReceiverList []*EncJobReceiver `json:"resultReceiverList"`
}


// EncPartyList
type PartyPrivate struct {
	ServerInfo *ServerInfo `json:"serverInfo"`
}

type EncParty struct {
	Party
	Private *PartyPrivate `json:"private"`
	Common  *Common       `json:"common"`
}

type EncPartyList struct {
	PartyList []*EncParty `json:"partyList"`
}
```

**JobInstance**
```go
type JobInstance struct {
	// job Id
	JobId string `json:"jobId"`
	// JobInstanceId
	JobInstanceId string `json:"jobInstanceId"`
	// JobInstance 名称
	JobInstanceName string `json:"jobInstanceName"`
	//  JobInstance类型 FQ(联邦查询)、FL(联邦学习)
	JobInstanceType     state.ModelType `json:"jobInstanceType"`
	JobInstanceImplType state.ImplType  `json:"jobInstanceImplType"`
	// Job状态  WAITING/APPROVED/READY/RUNNING/CANCELED/FAILED/SUCCESS
	Status state.JobInstanceState `json:"status"`
	// Job提交者Id
	Submitter string `json:"submitter"`
	// 状态变更时间戳
	UpdateTime string `json:"updateTime"`
	// 创建日期时间戳
	CreateTime string `json:"createTime"`
	// 请求数据原文
	RequestData string `json:"requestData"`
	// 根据原文生成的task的DAG
	TasksDAG string `json:"tasksDAG"`
	// 参与方Id数组
	PartyList []string `json:"partyList"`
	// 失败原因
	FailedReason string `json:"failedReason"`
	// 是否开启网关通讯
	GatewayEnable bool      `json:"gatewayEnable"`
	Dag           *Dag      `json:"dag"`
	Topology      *Topology `json:"topology"`
}
```


#### Job，JobInstance，Task及Service之间的关系
1. 调用合约预先生成唯一JobId，调用CreateJob方法，创建Job。
2. 调用合约预先先生成唯一的JobInstanceID，用CreateJob方法中的JobId作为入参，调用CreateJobInstance方法，将 Job与JobInstance进行关联。 
同时，Task列表及Service列表作为入参将JobInstance与Task和Service列表进行关联。
3. 每一个Task作为该Job的子任务,每一个Service作为该Job的子服务。

#### 实体及其关系

1. **Job（作业）**  
   表示任务或计算的元数据，定义了其结构、配置和相关资源。
    - 包含：
        - `JobTask[]`：属于该 Job 的任务（`JobTask`）列表。
        - `JobService[]`：该 Job 使用的服务（`JobService`）列表。
        - 其他元数据如 `JobId`、`JobName`、`ModelType`、`ProcessType` 等。

2. **JobInstance（作业实例）**  
   表示 Job 的一个执行实例。
    - 包含：
        - 通过 `JobId` 引用其所属的 Job。
        - 执行中的任务（`Task`）和服务（`Service`）实例。
        - 状态跟踪属性如 `Status`、`TasksDAG` 等。

3. **Task（任务）**  
   表示 `JobTask` 的一个运行时实例。
    - 包含：
        - 通过 `JobInstanceId` 引用其所属的 Job 实例。
        - 执行状态、输入、输出及参与者的信息。

4. **Service（服务）**  
   表示 `JobService` 的一个运行时实例。
    - 包含：
        - 通过 `JobInstanceId` 引用其所属的 Job 实例。
        - 暴露的端点、配置和状态的信息。

5. **JobTask（作业任务元数据）**  
   表示 Job 中一个特定任务的元数据。
    - 包含：
        - 描述、版本、输入/输出详情及参与者信息。

6. **JobService（作业服务元数据）**  
   表示 Job 中一个特定服务的元数据。
    - 包含：
        - 描述、暴露的端点及配置信息。

---

#### ER图参考
![CM-CS-241230-MPC.png](../images/CM-CS-241230-MPC.png)
#### 状态机描述

状态列表及状态转换规则都支持自定义，以下为示例

**statusList**
```go
var statusList = map[int]string{
    0: "WAITING", // 等待中
    1: "INIT", // 初始化
    2: "SETUP", // 创建
    3: "READY", // 等待
    4: "RUNNING", // 运行中
    5: "SUCCESS", // 成功
    6: "CANCELED", // 取消
    7: "FAILED", // 失败
```
状态枚举值对应的英文含义

**changeStatusMap**
```go
var changeStatusMap = map[int][]int{
	  5:{1,2,3}, // SUCCESS <= INIT/SETUP/READY
	  7:{2,3,4,5}, // FAILED <= SETUP/READY/RUNNING/SUCCESS
  }
```
* 状态转换规则，map的key为目标状态，valueinstance为匹配的原始状态列表，在调用SetJobInstanceStatus，SetTaskStatus时，入参的参数status（修改的目标状态），会和 changeStatusMap进行匹配，作为校验。
* 举例判断规则：
假设当前状态status = 1，调用 在调用SetJobStatus(dstStatus=5)，匹配规则
statusList = changeStatusMap[5]，然后判断status=1是否在statusList列表中。

* 参考以上，自定义的状态枚举值列表及状态转换规则，作为参数，在创建合约调用InitJobStatus，InitsInstanceStatus，InitTaskStatus的方法，会将
状态枚举值列表及状态转换规则存下来，方便后续使用。

### 函数分类
| 函数分类                     | 函数类说明                          | 
|--------------------------|--------------------------------|
| JobMethods               | job函数集，包含job的查询，查询列表，创建等方法     |
| JobInstanceMethods       | job实例函数集，包含job实例的查询，查询列表，创建等方法 |
| TaskMethods              | task函数集，包含task的查询，查询列表等方法      |
| ComputingResourceMethods | 计算资源函数集，包含计算资源的创建，查询等主要方法      |
| ComputingModelMethods    | 计算模型函数集，包含计算模型的创建，查询等方法        | 


## 协议详情

### 基础方法介绍

#### 合约函数集

##### JobMethods 相关方法
| 函数名                    | 函数类型   | 函数说明               | 备注        | 是否必须按 |
|------------------------|--------|--------------------|-----------|------|
| CreateJob              | 调用     | 创建job              | 任何人可调用    | 必选   |
| CreateJobApprove       | 调用     | 创建任务审批             | Job参与方可调用 | 必选   |
| UpdateJobTriggerEnable | 调用     | 修改triggerEnable 为1 | Job创建者可调用 | 必选   |
| GetJob                 | 查询     | 通过jobId查询job       | Job参与者可调用 | 必选   |
| GetJobApprove          | 查询     | 通过jobId查询job审批信息   | Job参与者可调用 | 必选   |
| GetJobList             | 查询     | 查询job信息列表          | 任何人可调用    | 非必选  |
| GetJobApproveList      | 查询     | 查询job审批信息列表        | 任何人可调用    | 非必选  |
| UpdateJob              | 调用     | 更新job信息            | Job创建者可调用 | 非必选  |
| SetJobFinish           | 调用     | 设置job 完成信息         | Job创建者可调用 | 必选   |


```go
type EncJobApproveExpandInfo struct {
	JobApproveExpandInfo
	EncService  *EncJobService       `json:"encService"`
	EncTaskList []*EncJobApproveTask `json:"encTaskList"`
}

type EncJobApproveTask struct {
	JobId    string `json:"jobId"`
	TaskName string `json:"taskName"`
	TaskId   string `json:"taskId"`
	Party    *Party `json:"party"`
}
```

```go
type JobApproveInfo struct {
    // job id
    JobId string `json:"jobId"`
    // 状态
    Status string `json:"status"`
    // 审批参与方数量
    ApprovedPartyNum int32 `json:"approvedPartyNum"`
    // 拒绝参与方数量
    RejectedPartyNum int32 `json:"rejectedPartyNum"`
    // 审批比率
    ApprovedRate int32 `json:"approvedRate"`
    // 审批参与方列表
    ApprovePartyInfoList []*ApprovePartyInfo `json:"approvePartyInfoList"`
    JobSummaryInfo       *JobSummaryInfo     `json:"jobSummaryInfo"`
    // 其他自定义扩展字段
    // ...
}
```

**CreateJob**
```go
//
// CreateJob func CreateJob 
//  @Description: 创建任务
//  @param Job
//  @return error
//  @event topic: CreateJob 创建job的事件
//  @event topic: CreateJobApprove 创建job的事件
CreateJob(Job *EncJob) error
```

**CreateJobApprove**
```go
//
// CreateJobApprove func CreateJobApprove
//  @Description: 创建任务审批
//  @param JobApproveExpandInfo
//  @return error
//  @event topic: CreateJobApprove 审批的事件
CreateJobApprove(JobApproveExpandInfo *EncJobApproveExpandInfo) error
```

**GetJob**
```go
//
// GetJob func GetJob
//  @Description: 使用计算任务id查询计算任务
//  @param jobId
//  @return EncJob job
//  @return error
//
GetJob(jobId string) (*EncJob, error)
```

**GetJobApprove**
```go
//
// GetJobApprove func GetJobApprove
//  @Description: 使用计算任务id查询审批信息
//  @param jobId
//  @return JobApproveInfo
//  @return error
//
GetJobApprove(jobId string) (*JobApproveInfo, error)
```


**UpdateJobTriggerEnable**
```go
//
// UpdateJobTriggerEnable func UpdateJobTriggerEnable
//  @Description: 设置Job状态
//  @param jobId
//  @param triggerEnable
//  @return error
//  @event topic: UpdateJobTriggerEnable 触发job的事件
UpdateJobTriggerEnable(jobId string, triggerEnable bool) error
```

**UpdateJob**
```go
//
// UpdateJob func UpdateJob
//  @Description: 更改Job信息
//  @param jobId
//  @param triggerEnable
//  @return error
//  @event topic: UpdateJobTriggerEnable 触发job的事件
UpdateJobTriggerEnable(jobId string, triggerEnable bool) error
```

**SetJobFinish**
```go
//
// SetJobFinish func SetJobFinish
//  @Description: 设置job 完成信息
//  @param jobId
//  @return error
//  @event topic: UpdateJobTriggerEnable 触发job的事件
SetJobFinish(jobId string) error
```

##### 事件
**EmitCreateJobApproveEvent**
```go
//
// EmitCreateJobApproveEvent func EmitCreateJobApproveEvent
//  @Description: 事件 topic: CreateJobApprove
//  @param jobApproveInfo 审批信息
//  @param  jobStatus job的状态
//  @param  gatewayInfoList 网关信息
//  @param  cardInfoList 板卡信息
//  @param  EncService 更新的常驻服务信息
//  @param  EncTaskList 更新的task列表
EmitCreateJobApproveEvent(jobApproveInfo *types.JobApproveInfo, jobStatus state.JobStatus, EncTaskList *types.EncJobTaskList, EncService *types.EncJobService, gatewayInfoList []*types.GatewayInfo, cardInfoList []*types.CardInfo)
```

**EmitCreateJobEvent**
```go
//
// EmitCreateJobEvent func EmitCreateJobEvent
//  @Description: 事件 topic: CreateJob
//  @param job 加密的job信息
//
EmitCreateJobEvent(job *EncJob)
```

**EmitUpdateJobTriggerEnableEvent**
```go
//
// EmitUpdateJobTriggerEnableEvent func EmitUpdateJobTriggerEnableEvent
//  @Description: 事件 topic: CreateJob
//  @param jobId job的id
//  @param jobTriggerEnable 是否触发job
EmitUpdateJobTriggerEnableEvent(jobId string, jobTriggerEnable bool)
```

**EmitSetJobFinishEvent**
```go
//
// EmitSetJobFinishEvent func EmitSetJobFinishEvent
//  @Description: 事件 topic: SetJobFinish
//  @param jobId job的id
//  @param isFinish job是否完成
EmitSetJobFinishEvent(jobId string, isFinish bool)
```

##### JobInstanceMethods 相关方法
| 函数名                  | 函数类型 | 函数说明                    | 备注     | 是否必选 |
|----------------------|------|-------------------------|--------|-----|
| SetJobInstanceStatus | 调用   | 更新job实例状态        | Job创建者可调用 | 必选  |
| CreateJobInstance    | 调用   | 创建jobInstance 实例        | Job创建者可调用 |必选  |
| CancelJobInstance    | 调用   | 取消jobInstance 实例        | Job创建者可调用 |必选  |
| GetJobInstance       | 查询   | 根据jobInstanceId 获取 实例   | Job参与方可调用 |必选  |
| GetJobInstanceList   | 查询   | 根据jobId 获取 实例列表         | Job参与方可调用 |必选  |


##### SetJobInstanceStatus

```go
// SetJobInstanceStatus func SetJobInstanceStatus 
//  @Description: 启动jobInstance
//  @param jobInstanceId 
//  @param partyId 参与方id
//  @param  jobInstanceState  jobInstance状状态
//  @return error
//  @event SetJobInstanceStatus
SetJobInstanceStatus(jobInstanceId string, partyId string, state JobInstanceState) error
```

**CreateJobInstance**
```go
//
// CreateJobInstance func CreateJobInstance
//  @Description:  创建jobInstance 实例
//  @param jobInstance job实例
//  @param taskList 任务列表
//  @param serviceList 服务列表
//  @return error
//  @event topic: CreateJobInstance
CreateJobInstance(jobInstance *EncJobInstance, taskList []*EncTask, serviceList []*EncService) error
```

**CancelJobInstance**
```go
//
// CancelJobInstance func CancelJobInstance
//  @Description:  取消jobInstance 实例
//  @param jobInstanceId 
//  @return error
//  @event topic: CancelJobInstance
CancelJobInstance(jobInstanceId string) error
```

```go
// GetJobInstance func GetJobInstance 
//  @Description: 获取jobInstance
//  @param jobInstanceId 
//  @param partyId 参与方id
//  @return JobInstanceInfo
//  @return error
GetJobInstance(jobInstanceId string) (*JobInstanceInfo, error)
```

```go
// GetJobInstanceList func GetJobInstanceList 
//  @Description: 获取jobInstance列表
//  @param jobInstanceId 
//  @param partyId 参与方id
//  @return JobInstanceInfo
//  @return error
GetJobInstanceList(jobInstanceId string) (*[]*JobInstanceInfo, error)
```

##### 事件
**EmitCreateJobInstanceEvent**
```go
//
// EmitCreateJobInstanceEvent func EmitCreateJobInstanceEvent
//  @Description: 事件 topic：CreateJobInstance
//  @param jobInstance jobInstance
//  @param task   加密的任务列表
//  @param service  常驻服务列表
//  @param processExecutedTimes  job已执行次数
//  @return error
EmitCreateJobInstanceEvent(jobInstance *types.EncJobInstance, task []*types.EncTask, service []*types.EncService, processExecutedTimes int) erro
```

**EmitSetJobInstanceStatusEvent**
```go
//
// EmitSetJobInstanceStatusEvent func EmitSetJobInstanceStatusEvent
//  @Description: 时间 topic：UpdateJobInstanceStatus
//  @param jobInstanceId 
//  @param  partyId  参与方id
//  @param  timestamp  时间戳
//  @param  partyJobInstanceStatus 参与方审批完的jobInstance状态
//  @param  jobInstanceStatus  jobInstance状状态
EmitSetJobInstanceStatusReadyEvent(jobInstanceId string, partyId string, timestamp string, partyJobInstanceStatus, jobInstanceStatus int)
```

**EmitCancelJobInstanceEvent**
```go
//
// EmitSetJobInstanceStatusEvent func EmitSetJobInstanceStatusEvent
//  @Description: 时间 topic：UpdateJobInstanceStatus
//  @param jobInstanceId 
//  @param jobId 
// @param jobInstanceStatusInfo 任务实例的状态列表
//  @param jobTriggerEnable job是否可触发
//  @param  timestamp  时间戳
//  @return error
EmitCancelJobInstanceEvent(jobInstanceId, jobId, timestamp string, jobInstanceStatusInfo *types.JobInstanceStatusInfo, jobTriggerEnable bool) error
```

##### TaskMethods 相关方法
| 函数名                 | 函数类型 | 函数说明                         | 备注        | 是否必选 |
|---------------------|------|------------------------------|-----------|------|
| SetTaskPartyStatus  | 调用   | 更新task的状态及失败原因               | Job参与者可调用 | 非必选  |
| UpdateTaskInfo      | 调用   | 获取task状态和输出结果                | Job参与者可调用 | 必选   |
| GetTaskList         | 查询   | 查询 taskList 根据 jobInstanceId | Job参与者可调用 | 非必选  |

**结构体**

```go
type TaskUpdateInfo struct {
	JobInstanceId string    `json:"jobInstanceId"`
	TaskId        string    `json:"taskId"` // 任务id
	OutputList    []*Output `json:"outputList"` // 任务输出信息
}
```


**SetTaskPartyStatus**
```go
//
// SetTaskPartyStatus func SetTaskPartyStatus
//  @Description: 只能修改sender对应patryId的任务状态和原因
//  @param jobInstanceId
//  @param taskId
//  @param partyId
//  @param reason
//  @return error
//  @event topic: SetTaskStatus
//  @event topic: UpdateJobInstanceFailedReason
SetTaskPartyStatus(jobInstanceId string, taskId string, partyId string, status int, reason string) error
```


**UpdateTaskInfo**
```go
//
// UpdateTaskInfo func UpdateTaskInfo
//  @Description: 只能修改sender对应patryId的任务状态和输出
//  @param jobInstanceId
//  @param taskId
//  @param partyId
//  @param reason
//  @return error
//  @event topic: SetTaskStatus
//  @event topic: SetTaskOutput  
UpdateTaskInfo(jobInstanceId string, taskId string, partyId string, status int, taskUpdateInfoList TaskUpdateInfo) error
```


**GetTaskList**
```go
//
// GetTaskListByJobInstanceId func GetTaskListByJobInstanceId
//  @Description: 查询tasklist
//  @param jobInstanceId
//  @return []*EncTask
//  @return error
//
GetTaskList(jobInstanceId string) ([]*EncTask, error)
```


##### 事件
**EmitSetTaskStatusEvent**
```go
//
// EmitSetTaskStatusEvent func EmitSetTaskStatusEvent
//  @Description: 事件 topic: SetTaskStatus
//  @param jobInstanceId
//  @param jobId
//  @param taskId 任务id
//  @param partyId 参与方id
//  @param timestamp 更新时间戳
//  @param taskPartyState 对应partyid的task状态
//  @param taskState task状态
//  @param jobInstanceState
//  @param jobTriggerEnable job是否可以触发
EmitSetTaskStatusEvent(jobInstanceId,  taskId, partyId, timestamp string, taskPartyState state.TaskPartyState, taskState state.TaskState, jobInstanceState state.JobInstanceState, jobId string, jobTriggerEnable bool) 
```


**EmitSetTaskOutputEvent**
```go
//
// EmitSetTaskOutputEvent func EmitSetTaskOutputEvent
//  @Description: 事件 topic: SetTaskStatus
//  @param jobInstanceId
//  @param taskId 任务id
//  @param partyId 参与方id
//  @param outputList 任务输出
EmitSetTaskOutputEvent(jobInstanceId string, taskId string, partyId string, outputList []*Output)
```

**EmitUpdateJobInstanceFailedReasonEvent**
```go
//
// EmitUpdateJobInstanceFailedReasonEvent func EmitUpdateJobInstanceFailedReasonEvent
//  @Description: 事件 topic: SetTaskStatus
//  @param jobInstanceId
//  @param taskId 任务id
//  @param partyId 参与方id
//  @param reason 失败原因
EmitUpdateJobInstanceFailedReasonEvent(jobInstanceId, taskId, partyId， reason string)
```

##### ServiceMethods 相关方法
| 函数名                               | 函数类型 | 函数说明                         | 备注        | 是否必选 |
|-----------------------------------|------|------------------------------|-----------|------|
| SetServiceFailed                  | 调用   | 更新service 失败状态               | Job参与者可调用 | 必选   |
| UpdateServiceStatus               | 调用   | 更新service的运行状态               | Job参与者可调用    | 必选   |
| GetServiceList                    | 查询   | 根据jobInstanceId获取serviceList | Job参与者可调用    | 非必选  |

**GetServiceList**
```go
//
// GetServiceListByJobInstanceId func GetServiceListByJobInstanceId
//  @Description: 查询servicelist
//  @param jobInstanceId
//  @return []*EncService
//  @return error
//
GetServiceList(jobInstanceId string) ([]*EncService, error)
```

**UpdateServiceStatus**
```go
//
// UpdateServiceStatus func UpdateServiceStatus
//  @Description: 只能修改sender对应patryId的service状态
//  @param jobInstanceId 
//  @param serviceId 服务id
//  @param status  状态
//  @param timestamp 更新的时间戳
//  @return error
//  @event topic: UpdateServiceStatus
UpdateServiceStatus(jobInstanceId string, serviceId string, timestamp string, status int) error
```

**SetServiceFailed**
```go
//
// SetServiceFailed func SetServiceFailed
//  @Description: 只能修改sender对应patryId的service 失败原因
//  @param jobInstanceId 
//  @param serviceId 服务id
//  @param partyId  参与方id
//  @param reason  失败原因
//  @param timestamp 更新的时间戳
//  @return error
//  @event topic: UpdateServiceStatus
//  @event topic: UpdateJobInstanceFailedReason
SetServiceFailed(jobInstanceId string, serviceId string, partyId string, reason string) error
```


##### 事件
**EmitUpdateServiceStatusEvent**
```go
//
// EmitUpdateServiceStatusEvent func EmitUpdateServiceStatusEvent
//  @Description: 事件 topic: SetTaskStatus
//  @param jobInstanceId
//  @param serviceId 任务id
//  @param jobId
//  @param serviceState service状态
//  @param jobInstanceState jobInstance状态
//  @param jobTriggerEnable job是否可以触发
EmitUpdateServiceStatusEvent(jobInstanceId, serviceId, jobId string, serviceState state.ServiceState, jobInstanceState state.JobInstanceState, jobTriggerEnable bool)
```

**EmitUpdateJobInstanceFailedReasonEvent**
```go
//
// EmitUpdateJobInstanceFailedReasonEvent func EmitUpdateJobInstanceFailedReasonEvent
//  @Description: 事件 topic: SetTaskStatus
//  @param jobInstanceId
//  @param taskId 任务id
//  @param partyId 参与方id
//  @param reason 失败原因
EmitUpdateJobInstanceFailedReasonEvent(jobInstanceId, taskId, partyId， reason string)
```

#### ComputingResourceMethods 相关方法

| 函数名                               | 函数类型 | 函数说明            | 备注         |
|-----------------------------------| -------- |-----------------| ------------ |
| CreateComputingResource           | 调用     | 提交计算资源          | 任何人可调用 |
| GetComputingResource              | 查询     | 获取计算资源          | 任何人可调用 |
| UnpublishComputingResource        | 调用     | 计算资源下架          | 计算资源创建方可调用 |
| PublishComputingResource          | 调用     | 计算资源重新上架        | 计算资源创建方可调用 |
| GetComputingResourceListByPartyId | 查询     | 通过 partyId 获取列表 | 任何人可调用 |
| GetComputingResourceList          | 查询     | 获取全部计算资源        | 任何人可调用 |

**结构体**

```go
type ComputingResourceGroup struct {
    // 参与方ID
    PartyId string `json:"partyId"`
    // 分组ID
    GroupId string `json:"groupId"`
    // 创建时间
    CreateTime string `json:"createTime"`
    // 更新时间
    UpdateTime string `json:"updateTime"`
    // 分组名称
    GroupName string `json:"groupName"`
    // 描述
    Description string `json:"description"`
    // 交易ID
    TransactionId string `json:"transactionId"`
    // 状态
    Status int `json:"status"`
    // 计算资源节点列表
    ComputingResourceNodeList []*ComputingResourceNode `json:"computingResourceNodeList"`
}

type ComputingResourceNode struct {
    // 分组ID
    GroupId string `json:"groupId"`
    // 节点ID
    NodeId string `json:"nodeId"`
    // 节点名称
    NodeName string `json:"nodeName"`
    // 节点地址
    NodeAddress string `json:"nodeAddress"`
    // 节点规格
    NodeSpec string `json:"nodeSpec"`
    // 状态
    Status int `json:"status"`
    // 创建时间
    CreateTime string `json:"createTime"`
    // 更新时间
    UpdateTime string `json:"updateTime"`
    // 计算资源安全卡列表
    ComputingResourceCardList []*ComputingResourceCard `json:"computingResourceCardList"`
}

type ComputingResourceCard struct {
    // 节点ID
    NodeId string `json:"nodeId"`
    // 安全卡序列号
    CardSerial string `json:"cardSerial"`
    // 安全卡型号
    CardModel string `json:"cardModel"`
    // 安全卡规格
    CardSpec string `json:"cardSpec"`
    // 安全卡版本
    CardVersion string `json:"cardVersion"`
    // 安全卡索引
    CardIndex int `json:"cardIndex"`
}
```

**CreateComputingResource**

```go
//
// CreateComputingResource 提交计算资源
//  @param computingResource
//  @return error
//
CreateComputingResource(computingResource *ComputingResourceGroup) error
```

**GetComputingResource**

```go
//
// GetComputingResource 获取计算资源
//  @param id 计算资源id
//  @param partyId 参与方ID
//  @return *ComputingResourceGroup
//  @return error
//
GetComputingResource(id string, partyId string) (*ComputingResourceGroup, error)
```

**UnpublishComputingResource**

```go
//
// UnpublishComputingResource 删除计算资源
//  @param 计算资源id
//  @param partyId 参与方ID
//  @return error
//
UnpublishComputingResource(id string, partyId string) error
```

**PublishComputingResource**

```go
//
// PublishComputingResource 撤销删除
//  @param id 计算资源id
//  @param partyId 参与方ID
//  @return error
//
PublishComputingResource(id string, partyId string) error
```

**GetComputingResourceListByPartyId**

```go
//
// GetComputingResourceListByPartyId 通过 partyId 获取列表
//  @param partyId 参与方ID
//  @param groupName 计算资源组名（可选参数）
//  @return []*ComputingResourceGroup
//  @return error
//
GetComputingResourceListByPartyId(partyId string, groupName []string) ([]*ComputingResourceGroup, error)
```

**GetComputingResourceList**

```go
//
// GetComputingResourceAll 获取全部计算资源
//  @param groupNameList 根据计算资源组名列表获取计算资源（可选参数）
//  @return []*ComputingResourceGroup
//  @return error
//
GetComputingResourceList(groupNameList []string) ([]*ComputingResourceGroup, error)
```
##### 事件

**EmitCreateComputingResourceEvent**
```go
//
// EmitCreateComputingResourceEvent func EmitCreateComputingResourceEvent 
//  @Description: 事件 topic:CreateComputingResource
//  @param computingResource
//
EmitCreateComputingResourceEvent(computingResource *ComputingResource)
```

**EmitUnpublishComputingResourceEvent**
```go
//
// EmitUnpublishComputingResourceEvent func EmitUnpublishComputingResourceEvent 
//  @Description: 事件 topic:UnpublishComputingResource
//  @param id
//  @param partyId
//
EmitUnpublishComputingResourceEvent(id string, partyId string)
```

**EmitPublishComputingResourceEvent**
```go
//
// EmitPublishComputingResourceEvent func EmitPublishComputingResourceEvent 
//  @Description: 事件 topic:PublishComputingResource
//  @param id
//  @param partyId
//
EmitPublishComputingResourceEvent(id string, partyId string)
```

#### ComputingModelMethods 相关方法

| 函数名                    | 函数类型 | 函数说明             | 备注         |
| ------------------------- | -------- | -------------------- | ------------ |
| CreateComputingModel      | 调用     | 提交机密计算模型     | 任何人可调用 |
| GetComputingModel         | 查询     | 获取机密计算模型     | 任何人可调用 |
| GetComputingModelList     | 查询     | 获取机密计算模型列表 | 任何人可调用 |
| UnpublishComputingModel   | 调用     | 机密计算模型下架     | 模型创建方可调用 |
| PublishComputingModel     | 调用     | 机密计算模型重新上架 | 模型创建方可调用 |

**结构体**

```go
type ConfidentialComputingModel struct {
    // 不可为空，机密计算模型ID
    ModelId string `json:"id"`
    // 参与方ID
    PartyId string `json:"partyId"`
    // 不可为空，模型名称
    ModelName string `json:"name"`
    // 不可为空，模型类型
    ModelType string `json:"type"`
    // 不可为空，模型版本号，如：v1.0
    ModelVersion string `json:"version"`
    // 可为空，所属类别：反垄断风险，数据安全风险，金融风险，平台企业用工风险，不正当竞争/价格行为风险，其它
    Category string `json:"category"`
    // 可为空，模型描述
    Description string `json:"description"`
    // 不可为空，模型执行程序文件内容
    ModelFile []byte `json:"modelFile"`
    // 不可为空，模型执行程序文件 Hash
    ModelFileHash string `json:"modelFileHash"`
    // 不可为空，模型源码文件内容
    SourceFile []byte `json:"sourceFile"`
    // 不可为空，模型源码文件 Hash
    SourceFileHash string `json:"sourceFileHash"`
    // 不可为空，方法名称
    MethodName string `json:"methodName"`
    // 不可为空，方法描述
    MethodDescription string `json:"methodDescription"`
    // 状态
    Status int `json:"status"`
    // 创建时间
    CreateTime string `json:"createTime"`
    // 模型参数列表
    ModelParameters []*ConfidentialComputingModelParameters `json:"modelParameters"`
    // 返回参数列表
    ReturnParameters []*ConfidentialComputingModelReturnParameters `json:"returnParameters"`
    // 其他自定义扩展字段
    // ...
}

type ConfidentialComputingModelParameters struct {
    // 模型ID
    ModelId string `json:"fid"`
    // 变量名称
    VariableName string `json:"name"`
    // 数据来源
    DataSource string `json:"dataSource"`
    // 数据类型
    DataType int `json:"dataType"`
    // 是否加密 0：否，1：是
    IsEncrypt int `json:"isEncrypt"`
    // 描述
    Description string `json:"description"`
}

type ConfidentialComputingModelReturnParameters struct {
    // 模型ID
    ModelId string `json:"fid"`
    // 参数名称
    ParameterName string `json:"name"`
    // 数据类型
    DataType int `json:"dataType"`
    // 是否必需 0：否，1：是
    IsRequired int `json:"isRequired"`
    // 描述
    Description string `json:"description"`
}
```

**CreateComputingModel**

```go
//
// CreateComputingModel 提交机密计算模型
//  @param computingModel
//  @return error
//
CreateComputingModel(computingModel *ConfidentialComputingModel) error
```

**GetComputingModel**

```go
//
// GetComputingModel 获取机密计算模型
//  @param id
//  @return *ConfidentialComputingModel
//  @return error
//
GetComputingModel(id string) (*ConfidentialComputingModel, error)
```

**GetComputingModelList**

```go
//
// GetComputingModelList 获取机密计算模型列表
//  @param createPartyId
//  @param startId
//  @param endId
//  @return []*ConfidentialComputingModel
//  @return error
//
GetComputingModelList(createPartyId string, startId string, endId string) ([]*ConfidentialComputingModel, error)
```

**UnpublishComputingModel**

```go
//
// UnpublishComputingModel 机密计算模型下架
//  @param modelId 机密计算模型ID
//  @param partyId 参与方ID
//  @return error
//
UnpublishComputingModel(modelId string, partyId string) error
```

**PublishComputingModel**

```go
//
// PublishComputingModel 机密计算模型重新上架
//  @param modelId 机密计算模型ID
//  @param partyId 参与方ID
//  @return error
//
PublishComputingModel(modelId string, partyId string) error
```

##### 事件
**EmitCreateComputingModelEvent**
```go
//
// EmitCreateComputingModelEvent func EmitCreateComputingModelEvent 
//  @Description: 事件 topic:ComputingModel
//  @param *ComputingModel
//
EmitCreateComputingModelEvent(ComputingModel *ComputingModel)
```
**EmitUnpublishComputingModelEvent**
```go
//
// EmitUnpublishComputingModelEvent func EmitUnpublishComputingModelEvent
//  @Description: 事件 topic: UnpublishComputingModel
//  @param modelId
//  @param partyId
//
EmitUnpublishComputingModelEvent(modelId string, partyId string)
```
**EmitPublishComputingModelEvent**
```go
//
// EmitPublishComputingModelEvent func EmitPublishComputingModelEvent
//  @Description: 事件 topic: PublishComputingModel
//  @param modelId
//  @param partyId
//
EmitPublishComputingModelEvent(modelId string, partyId string)
```


### 可拓展方法介绍

#### 合约函数集
| 函数名                             | 函数类型    | 函数说明                                                 | 备注           | 是否必选 |
|---------------------------------|---------|------------------------------------------------------|--------------|------|
| UpdateJobInstanceFailedReason   | 调用      | 更新job instance 失败原因                                  | 可选 | 非必选  |



**SetTaskOutput**
```go
//
// SetTaskOutput func SetTaskOutput
//  @Description: 设置 task 的输出
//  @param jobInstanceId
//  @param taskId
//  @param partyId
//  @param output
//  @return error
//  @event topic: SetTaskOutput
SetTaskOutput(jobInstanceId string, taskName string, partyId string, output []*Output) error
```

**UpdateJobInstanceFailedReason**
```go
//
// UpdateJobInstanceFailedReason func UpdateJobInstanceFailedReason
//  @Description: 更新  job failed reason
//  @param jobInstanceId
//  @param reason
//  @return error
//
UpdateJobInstanceFailedReason(jobInstanceId string, reason string) error
```

## 示例合约

[CMMPC-go](https://git.chainweaver.org.cn/chainweaver/mira/mira-contract)