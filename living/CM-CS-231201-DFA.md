## CM-CS-231201-DFA（CMDFA-2）同质化数字资产类合约标准协议整体介绍

### 背景及目的
长安链同质化数字资产合约标准`CM-CS-231201-DFA`全称`Chainmaker - Contract Standard - Digital Fungible Assets-231201版`简称`CMDFA-2`，
对标以太坊中的ERC20合约标准，使用该标准实现的合约可以直接对接其他支持该标准的合约之间的相互调用，也能对接支持该标准的钱包、区块链浏览器等生态工具。
CMDFA-1：[CMDFA-1](../history/CM-CS-221221-DFA.md)

### 适用场景
本标准支持Go、Rust、C++和TinyGo等除Solidity外的多种合约开发语言，适用于积分、游戏币等场景。


###  修订记录
#### CMDFA-1
* 修订内容：同质化资产合约标准第一版
#### CMDFA-2
* 修订内容：新增Metadata查询方法为可选方法，通过该方法可查询Token的Logo、描述、和既有的Name、Symbol、Decimals等方法返回的信息。


## 协议详情

### 综述
> 说明：由于长安链合约接收的参数都是[string:bytes]的KeyValue键值对类型，所以下面所有函数的参数中参数名表示Key的值，string表示字符串直接转换为Bytes，而"uint256","uint8"等表示将数值转换为十进制的字符串，然后字符串转换为Bytes，返回值也是同样处理

### 基础方法介绍


#### 合约函数集

| 函数名          | 函数类型 | 函数说明            | 备注                 |
|--------------|------|-----------------|--------------------|
| Name         | 查询   | 查询Token的完整名称    | 可选                 | 
| Symbol       | 查询   | 查询Token的简写符号    | 可选                 | 
| Decimals     | 查询   | 查询Token支持的小数位数  | 可选                 | 
| TotalSupply  | 查询   | 返回Token的发行总量 | 必须                 | 
| BalanceOf    | 查询   | 返回指定账户的余额 | 必须                 | 
| Transfer     | 调用   | 转账当前调用账户下的指定amount金额给to账户   | 必须，成功则触发transfer事件 | 
| TransferFrom | 调用   | 转账from账户下的指定amount金额给to账户   | 必须，成功则触发transfer事件 | 
| Approve      | 调用   | 当前调用者授权指定的spender账户可以动用自己名下的amount金额给使用   | 必须，成功则触发approve事件  | 
| Allowance    | 查询   | 查询指定owner账户授权有多少授权额度给spender可用   | 必须                 | 

##### Name

```go
// Name 查询Token的完整名称
// @return string
Name() string
```

##### Symbol

```go
// Symbol 查询Token的简写符号
// @return string
Symbol() string
```

##### Decimals

```go
// Decimals 查询Token支持的小数位数
// @return uint8 返回支持的小数位数
Decimals() uint8
```

##### TotalSupply

```go
// TotalSupply 查询Token的发行总量
// @return uint256 返回发行的Token总量
TotalSupply() uint256
```

##### BalanceOf

```go
// BalanceOf 查询账户的Token余额
// @param account 指定要查询余额的账户
// @return uint256 返回指定账户的余额
BalanceOf(account string) uint256
```

##### Transfer

```go
// Transfer 转账
// @param to 收款账户
// @param amount 转账金额
// @return bool 转账是否成功
// @return error 转账失败则返回Status：ERROR，Message具体错误
Transfer(to string, amount uint256) error
```

##### TransferFrom

```go
// TransferFrom 转账from账户下的指定amount金额给to账户
// @param from 转出账户
// @param to 转入账户
// @param amount 转账金额
// @return bool 转账是否成功
// @return error 转账失败则返回Status：ERROR，Message具体错误
TransferFrom(from string, to string, amount uint256) error
```

##### Approve

```go
// Approve 当前调用者授权指定的spender账户可以动用自己名下的amount金额给使用
// @param spender 被授权账户
// @param amount 授权使用的金额
// @return bool 授权是否成功
// @return error 授权失败则返回Status：ERROR，Message具体错误
Approve(spender string, amount uint256) error
```

##### Allowance

```go
// Allowance 查询owner授权多少额度给spender
// @param owner 授权人账户
// @param spender 被授权使用的账户
// @return uint256 返回授权金额
Allowance(owner string, spender string) uint256
```

### 可拓展方法介绍

#### 合约函数集
| 函数名               | 函数类型 | 函数说明              | 备注              |
| ---                 | ---     |-------------------|-----------------|
| Mint                | 调用     | 铸造发行Token         | 可选，铸造成功触发Mint事件 | 
| Burn                | 调用     | 销毁个人名下指定数量的Token  | 可选，销毁成功触发Burn事件 | 
| BurnFrom                | 调用     | 销毁某个账户下指定数量的Token | 可选，销毁成功触发Burn事件 | 
| Metadata                | 查询     | 查询Token的Logo、描述、和既有的Name、Symbol、Decimals等方法返回的信息 | 可选 |

##### Mint

```go
// Mint 铸造发行新Token
// @param account 发行到指定账户
// @param amount 铸造发行新Token的数量
// @return bool 铸造是否成功
// @return error 发行失败则返回Status：ERROR，Message具体错误
Mint(account string, amount uint256) error
```

##### Burn

```go
// Burn 销毁当前用户名下指定数量的Token
// @param amount 要销毁的Token数量
// @return bool 销毁是否成功
// @return error 销毁失败则返回Status：ERROR，Message具体错误
Burn(amount uint256) error
```

##### BurnFrom
```go
// BurnFrom 从spender账户名下销毁指定amount数量的Token
// @param spender 被销毁Token的所属账户
// @param amount 要销毁的数量
// @return bool 销毁是否成功
// @return error 销毁失败则返回Status：ERROR，Message具体错误
BurnFrom(spender string, amount uint256) error
```

##### Metadata

```go
// Metadata 查询Token的元数据信息
// @return string 返回Token的Logo、描述、和既有的Name、Symbol、Decimals等方法返回的信息的JSON字符串
Metadata() string
```
Metadata结构示例参考如下：
```json
{
    "image": "https://www.chainmaker.org.cn/logo.png",
    "description": "这是一个测试Token",
    "name": "测试Token",
    "symbol": "TEST",
    "decimals": 18,
    "totalSupply": 1000000000000000000000000000
}

```

## 事件
### Topic: Transfer
data:
* spender string
* to string
* amount uint256

### Topic: Approve
data:
* owner string
* spender string
* amount uint256

### Topic: Mint
data:
* account string
* amount uint256

### Topic: Burn
data:
* account string
* amount uint256

## 参考
[ERC20合约标准协议](https://github.com/ethereum/EIPs/blob/master/EIPS/eip-20.md)

## 示例合约
[CMDFA-go](https://git.chainmaker.org.cn/contracts/contracts-go/-/tree/master/standard-dfa)