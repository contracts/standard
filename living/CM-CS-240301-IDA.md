## CM-CS-240301-IDA（CMIDA-1）可认证数据资产（IDA）合约标准协议整体介绍

### 背景及目的
长安链可认证数据资产（IDA）合约标准`CM-CS-240301-IDA`全称`Chainmaker - Contract Standard - Identifiable Data Asset - 240301版`简称`CMIDA-1`，是一种用于可认证数据资产的标准。使用该标准实现的合约可以实现数据资产的创建、更新、删除、查询和认证，也可以通过不同的用户权限实现用户和数据资产权限的管理。

### 适用场景
本标准支持Go、Rust、C++和TinyGo等除Solidity外的多种合约开发语言，适用于数字资产登记、授权、认证等场景。


### 修订记录


#### CMIDA-1

* 修订内容：合约标准第一版

## 协议详情

### 结构体介绍

#### IDA数据资产结构

```go
// IDAInfo 定义了数据资产的主要结构体
type IDAInfo struct {
	Basic          Basic                `json:"basic"`
	Ownership      Ownership            `json:"ownership"`
	Source         Source               `json:"source,omitempty"`
	Scenarios      Scenarios            `json:"scenarios,omitempty"`
	Supply         Supply               `json:"supply,omitempty"`
	Details        Details              `json:"details,omitempty"`
	Status         Status               `json:"status,omitempty"`
	Columns        []ColumnInfo         `json:"columns,omitempty"`
	APIs           []APIInfo            `json:"apis,omitempty"`
	Certifications []CertificationInfo  `json:"certifications,omitempty"`
}

// Basic 定义了数据资产的基本信息结构
type Basic struct {
	ID              string       `json:"id"`                    // 资产的唯一登记编号
	Name            string       `json:"name"`                  // 资产的名称
	EnName          string       `json:"enName"`                // 资产英文名称
	Tags            []string     `json:"tags,omitempty"`        // 资产相关的关键词
	FileAttachments []Attachment `json:"attachments,omitempty"` // 资产相关的文件附件
	Category        int          `json:"category"`              // 资产的类型，1: 数据集, 2: API服务, 3: 数据报告, 4: 数据应用, 5: 计算模型
	Industry        Industry     `json:"industry"`              // 资产所属的行业代码
	Summary         string       `json:"summary,omitempty"`     // 资产的简介
	Creator         string       `json:"creator,omitempty"`     // 资产创建者的链上地址
	TxID            string       `json:"txID,omitempty"`        // 资产创建的交易ID
}

// Attachment 定义了附件资料的结构
type Attachment struct {
	Hash      string     `json:"hash,omitempty"`       // 资料哈希值
	Url       string     `json:"url,omitempty"`        // 资料的URL地址
	Type      int        `json:"type,omitempty"`       // 附件类型，1: 图片, 2: 合规证明材料, 3: 估值证明材料, 4: 其他相关附件
	Size      int        `json:"size,omitempty"`       // 资料的文件大小
	CreatedAt *time.Time `json:"created_at,omitempty"` // 附件创建时间
	UpdatedAt *time.Time `json:"updated_at,omitempty"` // 附件更新时间
	DeletedAt *time.Time `json:"deleted_at,omitempty"` // 附件删除时间
	Auditor   string     `json:"auditor,omitempty"`    // 审计者的链上地址
}

// Industry 定义了行业分类的结构
type Industry struct {
	Id    int    `json:"id"`              // id
	Code  string `json:"code,omitempty"`  // 行业编号
	Title string `json:"title,omitempty"` // 行业名称
}

// Ownership 定义了数据资产的所有权和使用情况
type Ownership struct {
	Holder    string          `json:"holder"`              // 资产持有者的链上地址
	Users     map[string]bool `json:"users,omitempty"`     // 资产加工使用者的链上地址列表
	Operators map[string]bool `json:"operators,omitempty"` // 资产经营者的链上地址列表
}

// Source 定义了数据资产来源的结构
type Source struct {
	Name             string       `json:"name,omitempty"`         // 源数据的名称
	Type             int          `json:"type,omitempty"`         // 数据的取得方式，1: 原始取得, 2: 收集取得, 3: 交易取得, 4: 其他方式
	Channel          string       `json:"channel,omitempty"`      // 数据的来源渠道
	ProofAttachments []Attachment `json:"attachments,omitempty"`  // 取得方式的证明文件
	UpdateCycle      UpdateCycle  `json:"update_cycle,omitempty"` // 数据的更新周期
}

// UpdateCycle 定义了数据资产的更新周期结构
type UpdateCycle struct {
	UpdateCycleType int    `json:"update_cycle_type"`           // 更新周期类型，1: 静态, 2: 实时, 3: 周期, 4：其他
	Cycle           int    `json:"cycle,omitempty"`             // 更新周期的具体数值
	UpdateCycleUnit int    `json:"update_cycle_unit,omitempty"` // 更新周期的单位，1: 分钟, 2: 小时, 3: 天
	Cron            string `json:"cron,omitempty"`              // 可扩展更新字段，可以使用cron表达式
}

// Scenarios 定义了数据资产适用的场景
type Scenarios struct {
	ApplicableScenarios string `json:"applicable_scenarios,omitempty"` // 适用场景描述
	ProhibitedScenarios string `json:"prohibited_scenarios,omitempty"` // 禁用场景描述
}

// Supply 定义了数据资产供应的结构
type Supply struct {
	ImmediatelySupply bool       `json:"immediately_supply,omitempty"`  // 是否即时供应
	DelayedSupplyTime *time.Time `json:"delayed_supply_time,omitempty"` // 延迟供应时间
}

// Details 定义了数据资产的具体细节
type Details struct {
	UserCategories []int      `json:"user_categories,omitempty"` // 使用对象类别，1: 政府用户, 2: 企业用户, 3: 个人用户, 4: 无限制用户
	Description    string     `json:"description,omitempty"`     // 资产的详细描述
	DataSample     string     `json:"data_sample,omitempty"`     // 数据产品的示例
	DataFormat     int        `json:"data_format,omitempty"`     // 数据的存储格式，1: 数据表格式, 2: Excel格式, 3: XML格式, 4: CSV格式, 5: JSON格式, 9: 其他格式
	TimeSpan       string     `json:"time_span,omitempty"`       // 数据的时间跨度
	CustomInfo     string     `json:"custom_info,omitempty"`     // 个性化信息汇总
	DataScale      DataScale  `json:"data_scale,omitempty"`      // 数据的规模描述
	ExpirationTime *time.Time `json:"expiration_time,omitempty"` // 资产的失效时间
}

// DataScale 定义了数据资产的规模
type DataScale struct {
	Type  int `json:"type"`  // 数据规模类型，1: 条, 2: M, 3: G
	Scale int `json:"scale"` // 数据规模数量
}

// Status 定义了数据资产的当前状态
type Status struct {
	Status              int        `json:"status"`                         // 资产的当前状态，1: 已创建, -1: 已删除
	CertificationStatus int        `json:"certification_status,omitempty"` // 认证状态，0: 待认证, 1: 认证中, 2: 认证完成
	CreatedAt           *time.Time `json:"created_at,omitempty"`           // 资产的创建时间
	UpdatedAt           *time.Time `json:"updated_at,omitempty"`           // 资产的更新时间
	DeletedAt           *time.Time `json:"deleted_at,omitempty"`           // 资产的删除时间
}

// ColumnInfo 定义了数据项的结构
type ColumnInfo struct {
	Name          string `json:"name"`                     // 数据项名称
	SecurityLevel int    `json:"security_level,omitempty"` // 分级分类
	DataType      string `json:"data_type"`                // 数据类型
	DataLength    int    `json:"data_length,omitempty"`    // 数据长度
	Description   string `json:"description,omitempty"`    // 字段描述
	DataExample   string `json:"data_example,omitempty"`   // 数据样例
	CustomColumn  string `json:"custom_column,omitempty"`  // 个性化字段
	CreatedAt     string `json:"created_at,omitempty"`     // 创建时间
	UpdatedAt     string `json:"updated_at,omitempty"`     // 更新时间
	IsPrimaryKey  int    `json:"is_primary_key,omitempty"` // 是否主键
	IsNotNull     int    `json:"is_not_null,omitempty"`    // 是否非空
	PrivacyQuery  int    `json:"privacy_query,omitempty"`  // 是否隐私计算
}

// APIInfo 定义了API接口的结构
type APIInfo struct {
	Url          string `json:"url"`                  // 接口地址
	Header       string `json:"header,omitempty"`     // 请求头
	Params       string `json:"params,omitempty"`     // 请求参数
	Response     string `json:"response,omitempty"`   // 返回参数
	ResponseType string `json:"resp_type"`            // 返回类型：JSON、XML等
	Method       string `json:"method"`               // 请求方式：GET、POST等
	CreatedAt    string `json:"created_at,omitempty"` // 创建时间
	UpdatedAt    string `json:"updated_at,omitempty"` // 更新时间
}

// CertificationInfo 定义了数据资产认证的结构
type CertificationInfo struct {
	Category       int          `json:"category"`                 // 审批类型，1：合规，2：价值评估
	Result         int          `json:"result"`                   // 认证结果，0：待认证，1：通过，2：驳回
	Explains       string       `json:"explains,omitempty"`       // 驳回原因
	Description    string       `json:"description,omitempty"`    // 认证描述
	RequestAddress string       `json:"requestAddress"`           // 申请人地址
	RequestTime    *time.Time   `json:"requestTime,omitempty"`    // 申请时间
	ExpireTime     *time.Time   `json:"expireTime,omitempty"`     // 过期时间
	HandleTime     *time.Time   `json:"handleTime,omitempty"`     // 处理时间
	CreatedAt      *time.Time   `json:"createdAt,omitempty"`      // 创建时间
	UpdatedAt      *time.Time   `json:"updatedAt,omitempty"`      // 更新时间
	DeletedAt      *time.Time   `json:"deletedAt,omitempty"`      // 删除时间
	Certifications []Attachment `json:"certifications,omitempty"` // 资产的认证文件
}
```

#### 用户相关结构
```go
type UserInfo struct {
	CreatedAt      *time.Time      `json:"created_at"`               // 创建时间
	Address        string          `json:"address"`                  // 用户地址
	UserType       int             `json:"userType"`                 // 用户类型，0: 管理员用户, 1: 普通用户
	UserPermission int             `json:"userPermission"`           // 用户权限
	IDAAmount      int             `json:"ida_amount"`               // 用户持有的IDA数量
	Approvals      map[string]bool `json:"approvals,omitempty"`      // 授权方的链上地址列表
    PlatformPubkey string          `json:"platformPubkey"`           // 关联的平台公钥，base64 编码字符串
}
```

#### 平台相关结构
```go
type PlatformInfo struct {
	Name            string           `json:"name"`            // 平台名称
	Pubkey          string           `json:"pubkey"`          // 平台公钥，base64 编码字符串
	CreatedAt       time.Time        `json:"created_at"`      // 创建时间
    Address         string           `json:"address"`         // 创建平台的链账户地址
	EnterpriseName  string           `json:"enterpriseName"`  // 企业名称
	EnterpriseTypes []int            `json:"enterpriseTypes"` // 使用平台的机构或者企业类型
	CreditCode      string           `json:"creditCode"`      // 企业信用编码
}
```

### 基础方法介绍

> 注：若入参为结构体：则为json格式字符串
>
> 若返回为结构体：则也是json格式字符串返回

#### 合约函数集

#### IDA相关

| 函数名                | 函数类型 | 函数说明                  | 备注             |
| --------------------- | -------- |-----------------------| ---------------- |
| CreateIDAs            | 调用     | 根据提供的详细信息创建一个或多个新的IDA | 需要登记权限     |
| UpdateIDA             | 调用     | 更新现有IDA的指定字段          | 需要持有者权限   |
| DeleteIDAs            | 调用     | 根据它们的ID删除一个或多个IDA     | 需要持有者权限   |
| CertifyIDA            | 调用     | 对IDA进行合规认证            | 需要认证权限     |
| QueryIDA              | 查询     | 根据其ID和指定字段检索IDA的信息    | 任何人可调用     |

**CreateIDAs**
```go
// CreateIDAs 根据提供的详细信息创建创建一个或多个新的IDA。
// @param idaInfos 必填，IDA列表。
// @return error 返回的错误信息。
// @event topic: IDACreatedEvent IDA创建成功时发出的事件，包含IDA的详细信息。
CreateIDAs(idaInfos []IDAInfo) error
```
**UpdateIDA**
```go
// UpdateIDA 更新现有IDA的指定字段。
// @param key 必填，IDA的唯一标识符。
// @param field 需要更新的字段名称，field为空时为更改整个IDAInfo。
// @param updates 必填，更新对应field的结构，field为空时为IDAInfo结构。
// @param updateAt 必填，更新发生的时间。
// @return error 返回的错误信息。
// @event topic: IDAUpdatedEvent IDA更新成功时发出的事件，包含更新的字段和内容。
UpdateIDA(key string, field string, updates []byte, updateAt time.Time) error
```
**DeleteIDAs**
```go
// DeleteIDAs 根据它们的ID删除一个或多个IDA。
// @param IDs 必填，要删除的IDA的ID列表。
// @return error 返回的错误信息。
// @event topic: IDADeletedEvent IDAs删除成功时发出的事件，包含被删除的IDAs的ID。
DeleteIDAs(IDs []string) error
```
**CertifyIDA**
```go
// CertifyIDA 根据提供的认证详情对IDA进行合规认证。
// @param ID 必填，要认证的IDA的ID。
// @param certification 必填，认证信息的结构体。
// @return error 返回的错误信息。
// @event topic: IDACertifiedEvent IDA认证成功时发出的事件，包含认证的详细信息。
CertifyIDA(ID string, certification CertificationInfo) error
```
**QueryIDA**
```go
// QueryIDA 根据其ID和指定field检索IDA的信息，如果field为空则返回全部IDA信息。
// @param ID 必填，要查询的IDA的ID。
// @param field 需要查询的字段名称，field为空时为查询整个IDAInfo。
// @return string 返回的IDA对应field的结构信息，field为空时返回整个IDAInfo。
// @return error 返回的错误信息。
QueryIDA(ID string, field string) (string, error)
```

#### 用户相关
| 函数名                | 函数类型 | 函数说明                  | 备注             |
| --------------------- | -------- |-----------------------| ---------------- |
| RegisterUser          | 调用     | 根据提供的详细信息注册一个新用户      | 任何人可调用     |
| ChangeUserType        | 调用     | 将现有用户的类型更改为指定的新类型     | 管理员权限       |
| ChangeUserPermissions | 调用     | 将现有用户的权限更改为指定的新权限     | 管理员权限       |
| QueryUser             | 查询     | 根据用户地址检索用户信息          | 任何人可调用     |

**RegisterUser**
```go
// RegisterUser 根据提供的详细信息注册一个新用户。
// @param user 必填，用户。
// @event topic: UserRegisteredEvent 用户注册成功时发出的事件，包含新用户的详细信息。
RegisterUser(user UserInfo) error
```
**ChangeUserType**
```go
// ChangeUserType 将现有用户的类型更改为指定的新类型。
// @param newType 必填，用户新的类型编号。
// @param userAddress 必填，用户的地址。
// @return error 返回的错误信息。
// @event topic: UserTypeChangedEvent 用户类型更改成功时发出的事件，包含新的用户类型信息。
ChangeUserType(newType int, userAddress string) error
```
**ChangeUserPermissions**
```go
// ChangeUserPermissions 将现有用户的权限更改为指定的新权限。
// @param newPermissions 必填，用户新的权限编号。
// @param userAddress 必填，用户的地址。
// @return error 返回的错误信息。
// @event topic: UserPermissionsChanged 用户权限更改成功时发出的事件，包含新的用户权限信息。
ChangeUserPermissions(newPermissions int, userAddress string) error
```
**QueryUser**
```go
// QueryUser 根据用户地址检索用户信息。
// @param userAddress 必填，用户的地址。
// @return string 返回的UserInfo。
// @return error 返回的错误信息。
QueryUser(userAddress string) (string, error)
```

#### 平台相关
| 函数名                | 函数类型 | 函数说明             | 备注             |
| --------------------- | -------- |------------------| ---------------- |
| RegisterPlatform          | 调用     | 根据提供的详细信息注册一个新平台 | 任何人可调用     |

**RegisterPlatform**
```go
// RegisterPlatform 根据提供的详细信息注册一个新平台。
// @param platform 必填，平台信息。
// @event topic: PlatformRegisteredEvent 平台注册成功时发出的事件，包含新平台的详细信息。
RegisterPlatform(platform PlatformInfo) error
```

### 可拓展方法介绍

#### 合约函数集

| 函数名                   | 函数类型 | 函数说明           | 备注             |
|-----------------------|--|----------------| ---------------- |
| Approve               | 调用 | 是否授权用户操作所有IDA  | 需要持有者权限   |
| SetIDAUser            | 调用 | 是否授予用户使用IDA  | 需要持有者或授权方权限 |
| SetIDAOperator        | 调用 | 是否授予用户经营IDA  | 需要持有者或授权方权限 |
| isApproved            | 查询 | 用户是否已被授权          | 任何人可调用   |
| isIDAUser             | 查询 | 查询某个用户是否有IDA使用权  | 任何人可调用 |
| isIDAOperator         | 查询 | 查询某个用户是否有IDA经营权 | 任何人可调用 |
| TransferHolder        | 调用 | 将IDA的所有权转移给另一个用户 | 需要持有者权限   |
| QueryHolder           | 查询 | 查询IDA的Holder | 任何人可调用   |
| ApplyForCertification | 调用 | 提交数据资产的认证申请    | 需要认证权限     |
| QueryUserIDAAmount    | 查询 | 检索用户持有的IDA数量   | 任何人可调用     |

**Approve**
```go
// Approve 授权或取消授权用户操作所有IDA。
// @param userAddress 必填，用户的地址。
// @param isApproved 必填，是否授权。
// @return error 返回的错误信息。
// @event topic: UserApprovalChangedEvent 授权状态变更时发出的事件，包含用户地址和授权状态。
Approve(userAddress string, isApproved bool) error
```

**SetIDAUser**
```go
// SetIDAUser 授予或移除用户使用IDA的权限。
// @param ID 必填，数据资产的ID。
// @param userAddress 必填，用户的地址。
// @param isAuthorized 必填，是否授权用户使用IDA。
// @return error 返回的错误信息。
// @event topic: IDAUserChangedEvent 用户使用权限变更时发出的事件，包含数据资产ID和用户地址。
SetIDAUser(ID string, userAddress string, isAuthorized bool) error
```

**SetIDAOperator**
```go
// SetIDAOperator 授予或移除用户经营IDA的权限。
// @param ID 必填，数据资产的ID。
// @param operatorAddress 必填，用户的地址。
// @param isAuthorized 必填，是否授权用户经营IDA。
// @return error 返回的错误信息。
// @event topic: IDAOperatorChangedEvent 用户经营权限变更时发出的事件，包含数据资产ID和用户地址。
SetIDAOperator(ID string, operatorAddress string, isAuthorized bool) error
```

**TransferHolder**
```go
// TransferHolder 将IDA的所有权转移给另一个用户。
// @param ID 必填，数据资产的ID。
// @param newHolderAddress 必填，新持有者的用户地址。
// @return error 返回的错误信息。
// @event topic: HolderTransferredEvent 所有权转移成功时发出的事件，包含数据资产ID和新持有者地址。
TransferHolder(ID string, newHolderAddress string) error
```

**QueryHolder**
```go
// QueryHolder 查询IDA的Holder。
// @param ID 必填，数据资产的ID。
// @return string 返回持有者的用户地址。
// @return error 返回的错误信息。
QueryHolder(ID string) (string, error)
```

**ApplyForCertification**
```go
// ApplyForCertification 提交数据资产的认证申请。
// @param ID 必填，数据资产的ID。
// @param authenticatorAddress 必填，申请认证的用户的区块链地址。
// @return error 返回的错误信息。
// @event topic: CertificationAppliedEvent 认证申请成功提交时发出的事件，包含数据资产ID和申请者地址。
ApplyForCertification(ID string, authenticatorAddress string) error
```

**QueryUserIDAAmount**
```go
// QueryUserIDAAmount 检索用户持有的IDA数量。
// @param userAddress 必填，用户的地址。
// @return int 返回用户持有的IDA数量。
// @return error 返回的错误信息。
QueryUserIDAAmount(userAddress string) (int, error)
```

### 事件

#### 基础事件介绍

**IDACreatedEvent**
```go
// EmitIDACreatedEvent 发出 IDA 创建成功的事件。
// @param idaInfos 必填，创建的IDA列表。
// @return (error) 发生错误时返回错误信息。
// @event topic: IDACreated 包含IDA的详细信息。
func EmitIDACreatedEvent(idaInfos []IDAInfo) error
```

**IDAUpdatedEvent**
```go
// EmitIDAUpdatedEvent 发出 IDA 更新成功的事件。
// @param key 必填，IDA的唯一标识符。
// @param field 需要更新的字段名称。
// @param updates 必填，更新对应field的内容。
// @param updateAt 必填，更新发生的时间。
// @return (error) 发生错误时返回错误信息。
// @event topic: IDAUpdated 包含更新的字段和内容。
func EmitIDAUpdatedEvent(key string, field string, updates []byte, updateAt time.Time) error
```

**IDADeletedEvent**
```go
// EmitIDADeletedEvent 发出 IDAs 删除成功的事件。
// @param IDs 必填，要删除的IDA的ID列表。
// @return (error) 发生错误时返回错误信息。
// @event topic: IDADeleted 包含被删除的IDAs的ID。
func EmitIDADeletedEvent(IDs []string) error
```

**IDACertifiedEvent**
```go
// EmitIDACertifiedEvent 发出 IDA 认证成功的事件。
// @param ID 必填，要认证的IDA的ID。
// @param certification 必填，认证信息的结构体。
// @return (error) 发生错误时返回错误信息。
// @event topic: IDACertified 包含认证的详细信息。
func EmitIDACertifiedEvent(ID string, certification CertificationInfo) error
```

**UserRegisteredEvent**
```go
// EmitUserRegisteredEvent 发出用户注册成功的事件。
// @param user 必填，用户信息。
// @return (error) 发生错误时返回错误信息。
// @event topic: UserRegistered 包含新注册用户的信息。
func EmitUserRegisteredEvent(user UserInfo) error
```

**UserTypeChangedEvent**
```go
// EmitUserTypeChangedEvent 发出用户类型更改成功的事件。
// @param newType 必填，用户新的类型编号。
// @param userAddress 必填，用户的地址。
// @return (error) 发生错误时返回错误信息。
// @event topic: UserTypeChanged 包含新的用户类型信息。
func EmitUserTypeChangedEvent(newType int, userAddress string) error
```
#### 可扩展事件介绍

**UserApprovalChangedEvent**
```go
// EmitUserApprovalChangedEvent 发出用户授权状态变更的事件。
// @param originAddress 必填，授权用户的地址。
// @param targetAddress 必填，被授权用户的地址。
// @param isApproved 必填，是否授权。
// @return (error) 发生错误时返回错误信息。
// @event topic: UserApprovalChanged 包含用户地址和授权状态。
func EmitUserApprovalChangedEvent(originAddress, targetAddress string, isApproved bool) error
```

**IDAUserChangedEvent**
```go
// EmitIDAUserChangedEvent 发出用户使用IDA权限变更的事件。
// @param ID 必填，数据资产的ID。
// @param userAddress 必填，用户的地址。
// @param isAuthorized 必填，是否授权用户使用IDA。
// @return (error) 发生错误时返回错误信息。
// @event topic: IDAUserChanged 包含数据资产ID和用户地址。
func EmitIDAUserChangedEvent(ID string, userAddress string, isAuthorized bool) error
```

**IDAOperatorChangedEvent**
```go
// EmitIDAOperatorChangedEvent 发出用户经营IDA权限变更的事件。
// @param ID 必填，数据资产的ID。
// @param operatorAddress 必填，用户的地址。
// @param isAuthorized 必填，是否授权用户经营IDA。
// @return (error) 发生错误时返回错误信息。
// @event topic: IDAOperatorChanged 包含数据资产ID和用户地址。
func EmitIDAOperatorChangedEvent(ID string, operatorAddress string, isAuthorized bool) error
```

**HolderTransferredEvent**
```go
// EmitHolderTransferredEvent 发出IDA所有权转移成功的事件。
// @param ID 必填，数据资产的ID。
// @param newHolderAddress 必填，新持有者的用户地址。
// @return (error) 发生错误时返回错误信息。
// @event topic: HolderTransferred 包含数据资产ID和新持有者地址。
func EmitHolderTransferredEvent(ID string, newHolderAddress string) error
```

**CertificationAppliedEvent**
```go
// EmitIDACertificationAppliedEvent 发出认证申请成功提交的事件。
// @param ID 必填，数据资产的ID。
// @param authenticatorAddress 必填，申请认证的用户的区块链地址。
// @return (error) 发生错误时返回错误信息。
// @event topic: IDACertificationApplied 包含数据资产ID和申请者地址。
func EmitIDACertificationAppliedEvent(ID string, authenticatorAddress string) error
```

## 示例合约

[CMIDA-go](https://git.chainweaver.org.cn/chainweaver/ida/contract-ida)