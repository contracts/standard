# 长安链合约标准协议-正式生效

包含如下合约标准：

[长安链同质化数字资产合约标准](./CM-CS-231201-DFA.md)

[长安链非同质化数字资产合约标准](./CM-CS-221221-NFA.md)

[长安链存证合约标准](./CM-CS-231201-Evidence.md)

[长安链身份认证合约标准](./CM-CS-221221-Identity.md)


## 示例合约

[长安链同质化数字资产合约](https://git.chainmaker.org.cn/contracts/contracts-go/-/tree/master/standard-dfa)

[长安链非同质化数字资产合约](https://git.chainmaker.org.cn/contracts/contracts-go/-/tree/master/standard-nfa)

[长安链存证合约](https://git.chainmaker.org.cn/contracts/contracts-go/-/tree/master/standard-evidence)

[长安链身份认证合约](https://git.chainmaker.org.cn/contracts/contracts-go/-/tree/master/standard-identity)

