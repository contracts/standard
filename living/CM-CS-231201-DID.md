## CM-CS-231201-DID（CMDID-1）去中心化数字身份（DID）合约标准协议整体介绍

### 背景及目的

长安链去中心化数字身份（DID）合约标准`CM-CS-231201-DID`全称`Chainmaker - Contract Standard - Decentralized Identifier - 231201版`简称`CMDID-1`，是一种用于实现分布式身份的标准。使用该标准实现的合约可以直接对接其他支持该标准的合约之间的相互调用，也能对接支持该标准的钱包、区块链浏览器等生态工具。

### 适用场景

本标准支持Go、Rust、C++和TinyGo等除Solidity外的多种合约开发语言，适用于数字身份、授权、验证等场景。

### 修订记录

#### CMDID-1

* 修订内容：合约标准第一版

## 协议详情

### 综述

> 说明：由于长安链合约接收的参数都是[string:bytes]的KeyValue键值对类型，所以下面所有函数的参数中参数名表示Key的值，string表示字符串直接转换为Bytes，而"int","uint"等表示将数值转换为十进制的字符串，然后字符串转换为Bytes，返回值也是同样处理。time.Time类型的参数和返回值都是转换为Unix时间戳的字符串，然后字符串转换为Bytes，bool类型的参数和返回值都是转换为字符串"true"或"false"，然后字符串转换为Bytes。

#### 对象

##### VC模板

```go
// VcTemplate vc模板
type VcTemplate struct {
	// Id 模板ID
	Id string `json:"id"`
	// Name 模板名称
	Name string `json:"name,omitempty"`
	// ShortName 模板简称
	ShortName string `json:"shortName,omitempty"`
	// Version 模板版本
	Version string `json:"version"`
	// VcType vc类型
	VcType string `json:"vcType,omitempty"`
	// Template 模板内容
	Template string `json:"template,omitempty"`
	// CreatorDid 创建者DID
	CreatorDid string `json:"creatorDid,omitempty"`
}
```

##### 代理信息

```go
// DelegateInfo 授权信息
type DelegateInfo struct {
	// DelegatorDid 授权者DID
	DelegatorDid string `json:"delegatorDid"`
	// DelegateeDid 被授权者DID
	DelegateeDid string `json:"delegateeDid"`
	// Resource 资源,一般是VcID
	Resource string `json:"resource"`
	// Action 操作，一般是"issue"或"verify"
	Action string `json:"action"`
	// StartTime 授权开始时间
	StartTime int64 `json:"startTime"`
	// Expiration 授权结束时间
	Expiration int64 `json:"expiration"`
}
```

#### VC颁发历史记录

```go
// VcIssueLog 记录vc发行日志
type VcIssueLog struct {
	// Issuer 发行者DID
	Issuer string `json:"issuer"`
	// Did vc持有者DID
	Did string `json:"did"`
	// TemplateId vc模板ID
	TemplateId string `json:"templateID"`
	// TemplateId vc模板Version
	TemplateVersion string `json:"templateVersion"`
	// VcID vcID或者vc hash
	VcID string `json:"vcID"`
	// IssueTime 发行上链时间
	IssueTime int64 `json:"issueTime"`
}
```

### 基础方法介绍

#### 合约函数集


| 函数名                  | 函数类型 | 函数说明            | 备注 |
| ----------------------- | -------- | ------------------- | ---- |
| DidMethod               | 查询     | 获取DID方法         | 必须 |
| IsValidDid              | 查询     | 判断DID是否合法     | 必须 |
| AddDidDocument          | 调用     | 添加DID文档         | 必须 |
| GetDidDocument          | 查询     | 根据DID获取DID文档  | 必须 |
| GetDidByPubkey          | 查询     | 根据公钥获取DID     | 必须 |
| VerifyVc                | 查询     | 验证vc              | 必须 |
| VerifyVp                | 查询     | 验证vp              | 必须 |
| EmitSetDidDocumentEvent | 事件     | 发送添加DID文档事件 | 必须 |
| RevokeVc                | 调用     | 撤销vc              | 必须 |
| GetRevokedVcList        | 查询     | 获取撤销vc列表      | 必须 |
| EmitRevokeVcEvent       | 事件     | 发送撤销vc事件      | 必须 |

以下是基础方法中每个函数的详细介绍：

##### DidMethod

```go
// DidMethod 获取DID方法，一个DID合约只有一个DID方法名，如did:cmid:1234567890这个DID，那么“cmid”就是DID方法名
// @return string 返回DID方法名称
DidMethod() string
```

##### IsValidDid

```go
// IsValidDid 判断DID是否合法
// @param did string 要判断的DID
// @return bool 返回DID是否合法
// @return error 如果DID不合法，返回Status：ERROR，Message具体错误
IsValidDid(did string) (bool, error)
```

##### AddDidDocument

didDocument是一个满足W3C DID标准的JSON字符串，形如：[单公私钥对应的DID文档](#section1)，如果是多个公钥对应的DID文档，可以参考[多公私钥对应的DID文档](#section2)。

```go
// AddDidDocument 添加DID文档
// @param didDocument string 要添加的DID文档
// @return error 添加失败则返回Status：ERROR，Message具体错误
AddDidDocument(didDocument string) error
```

##### GetDidDocument

```go
// GetDidDocument 根据DID获取DID文档
// @param did string 要查询的DID
// @return string 返回查询到的DID文档
// @return error 查询失败则返回Status：ERROR，Message具体错误
GetDidDocument(did string) (string, error)
```

##### GetDidByPubkey

```go
// GetDidByPubkey 根据公钥获取DID
// @param pk string 公钥
// @return string 返回对应的DID
// @return error 查询失败则返回Status：ERROR，Message具体错误
GetDidByPubkey(pk string) (string, error)
```

##### VerifyVc

VC为Verifiable Credential的缩写，表示可验证的凭证，是一种数字凭证，用于证明某个实体的某些属性，例如个人实名认证VC，可以证明某个实体的姓名、身份证号、手机号等属性，VC的格式参考[个人实名认证VC](#section3)。
VC内容包含用户隐私信息，不可打包上链，验证VC通过Query实现。

```go
// VerifyVc 验证vc
// @param vcJson string 要验证的vc JSON字符串
// @return bool 返回验证结果，true表示验证成功，false表示验证失败
// @return error 验证失败则返回Status：ERROR，Message具体错误
VerifyVc(vcJson string) (bool, error)
```

##### VerifyVp

VP为Verifiable Presentation的缩写，表示可验证的展示，是一种数字凭证，用于证明某个实体的某些属性，例如个人实名亮证，可以证明某个实体的姓名、身份证号、手机号等属性，VP的格式参考[个人实名亮证](#section4)。
VP内容包含用户隐私信息，不可打包上链，验证VP通过Query实现。

```go
// VerifyVp 验证vp
// @param vpJson string 要验证的vp JSON字符串
// @return bool 返回验证结果，true表示验证成功，false表示验证失败
// @return error 验证失败则返回Status：ERROR，Message具体错误
VerifyVp(vpJson string) (bool, error)
```

##### EmitSetDidDocumentEvent

```go
// EmitSetDidDocumentEvent 发送添加DID文档事件
// @param did string 被添加的DID
// @param didDocument string 被添加的DID文档
EmitSetDidDocumentEvent(did string, didDocument string)
```

##### RevokeVc

```go
// RevokeVc 撤销vc
// @param vcID string 要撤销的vc ID，因为vc是否具有全局唯一的vcID是由业务方自己保证的，所以如果没有vcID可以使用vc的hash值作为vcID
// 如果要批量撤销vc，可以使用参数：vcIDs string，其值是一个字符串数组的json形式。
// @return error 撤销失败则返回Status：ERROR，Message具体错误
RevokeVc(vcID(s) string) error
```

##### GetRevokedVcList

```go
// GetRevokedVcList 获取撤销vc列表
// @param vcIDSearch string 搜索的vc ID关键字
// @param start int 开始的索引，从0开始
// @param count int 要获取的数量，默认0表示获取所有
// @return []string 返回撤销的vc ID数组
// @return error 获取失败则返回Status：ERROR，Message具体错误
GetRevokedVcList(vcIDSearch string, start int, count int) ([]string, error)
```

##### EmitRevokeVcEvent

```go
// EmitRevokeVcEvent 发送撤销vc事件
// @param vcID string 被撤销的vc ID，如果是批量撤销VC，那么其参数是vcIDs字符串数组的json格式
EmitRevokeVcEvent(vcID string)
```

以上是基础方法中每个函数的详细介绍。

### 可拓展方法介绍

#### 合约函数集


| 函数名                     | 函数类型 | 函数说明                              | 备注 |
| -------------------------- | -------- | ------------------------------------- | ---- |
| GetDidByAddress            | 查询     | 根据地址查询DID                       | 可选 |
| UpdateDidDocument          | 调用     | 更新DID文档                           | 可选 |
| AddBlackList               | 调用     | 添加黑名单                            | 可选 |
| DeleteBlackList            | 调用     | 删除黑名单                            | 可选 |
| GetBlackList               | 查询     | 获取黑名单                            | 可选 |
| EmitAddBlackListEvent      | 事件     | 发送添加黑名单事件                    | 可选 |
| EmitDeleteBlackListEvent   | 事件     | 发送删除黑名单事件                    | 可选 |
| SetTrustRootList           | 调用     | 设置信任根列表                        | 可选 |
| GetTrustRootList           | 查询     | 获取信任根列表                        | 可选 |
| EmitSetTrustRootListEvent  | 事件     | 发送设置信任根列表事件                | 可选 |
| AddTrustIssuer             | 调用     | 添加信任的发行者                      | 可选 |
| DeleteTrustIssuer          | 调用     | 删除信任的发行者                      | 可选 |
| GetTrustIssuer             | 查询     | 获取信任的发行者                      | 可选 |
| EmitAddTrustIssuerEvent    | 事件     | 发送添加信任的发行者事件              | 可选 |
| EmitDeleteTrustIssuerEvent | 事件     | 发送删除信任的发行者事件              | 可选 |
| Delegate                   | 调用     | DIDA给DIDB授权其代理使用DIDA VC的权限 | 可选 |
| EmitDelegateEvent          | 事件     | 发送授权事件                          | 可选 |
| RevokeDelegate             | 调用     | 撤销授权                              | 可选 |
| EmitRevokeDelegateEvent    | 事件     | 发送撤销授权事件                      | 可选 |
| GetDelegateList            | 查询     | 查询授权列表                          | 可选 |
| SetVcTemplate              | 调用     | 设置VC模板                            | 可选 |
| GetVcTemplate              | 查询     | 获取VC模板                            | 可选 |
| GetVcTemplateList          | 查询     | 获取VC模板列表                        | 可选 |
| EmitSetVcTemplateEvent     | 事件     | 发送设置VC模板事件                    | 可选 |
| DisableVcTemplate          | 调用     | 禁用vc模板                            | 可选 |
| EnableVcTemplate           | 调用     | 启用vc模板                            | 可选 |
| GetDisabledVcTemplateList  | 查询     | 获取禁用vc模板列表                    | 可选 |
| EmitDisableVcTemplateEvent | 事件     | 发送禁用vc模板事件                    | 可选 |
| EmitEnableVcTemplateEvent  | 事件     | 发送启用vc模板事件                    | 可选 |
| VcIssueLog                 | 调用     | 记录VC颁发历史                        | 可选 |
| GetVcIssueLogs             | 查询     | 获取VC颁发历史                        | 可选 |
| EmitVcIssueLogEvent        | 事件     | 发送VC颁发历史事件                    | 可选 |
| GetVcIssuers               | 查询     | 根据DID获取VC颁发者列表               | 可选 |

下面是每个函数的详细介绍：

##### GetDidByAddress

```go
// GetDidByAddress 根据地址获取DID
// @param address string 地址
// @return string 返回对应的DID
// @return error 查询失败则返回Status：ERROR，Message具体错误
GetDidByAddress(address string) (string, error)
```

##### UpdateDidDocument

```go
// UpdateDidDocument 更新DID文档
// @param didDocument string 更新的DID文档
// @return error 更新失败则返回Status：ERROR，Message具体错误
UpdateDidDocument(didDocument string) error
```

##### AddBlackList

```go
// AddBlackList 添加黑名单，黑名单中的DID将无法通过VC VP验证，也无法获取DID文档
// @param dids []string 要添加到黑名单的DID数组
// @return error 添加失败则返回Status：ERROR，Message具体错误
AddBlackList(dids []string) error
```

##### DeleteBlackList

```go
// DeleteBlackList 删除黑名单
// @param dids []string 要从黑名单中删除的DID数组
// @return error 删除失败则返回Status：ERROR，Message具体错误
DeleteBlackList(dids []string) error
```

##### GetBlackList

```go
// GetBlackList 获取黑名单
// @param didSearch string 搜索的DID关键字
// @param start int 开始的索引，默认从0开始
// @param count int 要获取的数量，默认0表示获取所有
// @return []string 返回黑名单中的DID数组
GetBlackList(didSearch string, start int, count int) ([]string, error)
```

##### EmitAddBlackListEvent

```go
// EmitAddBlackListEvent 发送添加黑名单事件
// @param dids []string 被添加到黑名单的DID数组
EmitAddBlackListEvent(dids []string)
```

##### EmitDeleteBlackListEvent

```go
// EmitDeleteBlackListEvent 发送删除黑名单事件
// @param dids []string 从黑名单中删除的DID数组
EmitDeleteBlackListEvent(dids []string)
```

##### SetTrustRootList

```go
// SetTrustRootList 设置信任根列表，每个DID Document有个签名人DID，根据签名人DID来判断是否信任，如果没有设置信任根列表，则默认信任所有DID文档的签发者
// @param dids []string 要设置的信任根DID数组
// @return error 设置失败则返回Status：ERROR，Message具体错误
SetTrustRootList(dids []string) error
```

##### GetTrustRootList

```go
// GetTrustRootList 获取信任根列表
// @return []string 返回信任根DID数组
// @return error 获取失败则返回Status：ERROR，Message具体错误
GetTrustRootList() (dids []string, err error)
```

##### EmitSetTrustRootListEvent

```go
// EmitSetTrustRootListEvent 发送设置信任根列表事件
// @param dids []string 被设置的信任根DID数组
EmitSetTrustRootListEvent(dids []string)
```

##### AddTrustIssuer

作为Issuer，其DID文档中应该包含service属性，持证人通过service属性中的endpoint来获取VC

```go
// AddTrustIssuer 添加信任的发行者，只有受信任的发行人签发的VC才能通过验证，如果没有设置信任发行者，则默认信任所有发行者
// @param dids []string 要添加为信任发行者的DID数组
// @return error 添加失败则返回Status：ERROR，Message具体错误
AddTrustIssuer(dids []string) error
```

##### DeleteTrustIssuer

```go
// DeleteTrustIssuer 删除信任的发行者
// @param dids []string 要从信任发行者中删除的DID数组
// @return error 删除失败则返回Status：ERROR，Message具体错误
DeleteTrustIssuer(dids []string) error
```

##### GetTrustIssuer

```go
// GetTrustIssuer 获取信任的发行者
// @param didSearch string 搜索的DID关键字
// @param start int 开始的索引，默认从0开始
// @param count int 要获取的数量，默认0表示获取所有
// @return []string 返回信任发行者的DID数组
GetTrustIssuer(didSearch string, start int, count int) ([]string, error)
```

##### EmitAddTrustIssuerEvent

```go
// EmitAddTrustIssuerEvent 发送添加信任的发行者事件
// @param dids []string 被添加为信任发行者的DID数组
EmitAddTrustIssuerEvent(dids []string)
```

##### EmitDeleteTrustIssuerEvent

```go
// EmitDeleteTrustIssuerEvent 发送删除信任的发行者事件
// @param dids []string 从信任发行者中删除的DID数组
EmitDeleteTrustIssuerEvent(dids []string)
```

##### Delegate

```go
// Delegate 给delegateeDid授权当前交易发起人的资源（VC）代理出示权限
// @param delegateeDid string 被授权的DID，不能为空，不能是自己
// @param resource string 被授权的资源，一般是某个vcID（或者vc Hash），为空则表示所有资源
// @param action string 被授权的操作，默认为空，表示所有操作
// @param expiration 授权的过期时间，默认为空(0)，表示永久授权
// @return error 授权失败则返回Status：ERROR，Message具体错误
Delegate(delegateeDid string, resource string, action string, expiration int64) error
```

##### EmitDelegateEvent

```go
// EmitDelegateEvent 发送授权事件
// @param delegatorDid string 授权者的DID
// @param delegateeDid string 被授权者的DID
// @param resource string 被授权的资源
// @param action string 被授权的操作
// @param startTime int64 授权的开始时间
// @param expiration int64 授权的过期时间
EmitDelegateEvent(delegatorDid string, delegateeDid string, resource string, action string,startTime int64, expiration int64)
```

##### RevokeDelegate

```go
// RevokeDelegate 撤销授权
// @param delegateeDid string 被撤销授权的DID
// @param resource string 被撤销授权的资源，必须与授权时的资源一致
// @param action string 被撤销授权的操作，必须与授权时的操作一致
// @return error 撤销失败则返回Status：ERROR，Message具体错误
RevokeDelegate(delegateeDid string, resource string, action string) error
```

##### EmitRevokeDelegateEvent

```go
// EmitRevokeDelegateEvent 发送撤销授权事件
// @param delegatorDid string 撤销授权者的DID
// @param delegateeDid string 被撤销授权者的DID
// @param resource string 被撤销授权的资源
// @param action string 被撤销授权的操作
EmitRevokeDelegateEvent(delegatorDid string, delegateeDid string, resource string, action string)
```

##### GetDelegateList

```go
// GetDelegateList 查询授权列表
// @param delegatorDid string 授权者的DID
// @param delegateeDid string 被授权者的DID
// @param resource string 被授权的资源
// @param action string 被授权的操作
// @param start int 开始的索引，默认从0开始
// @param count int 要获取的数量，默认0表示获取所有
// @return []*DelegateInfo 返回授权列表
// @return error 查询失败则返回Status：ERROR，Message具体错误
GetDelegateList(delegatorDid string, delegateeDid string, resource string, action string, start int, count int) ([]*DelegateInfo, error)

```

##### SetVcTemplate

vcTemplate字段（VC模板）是一个JSON schema，在验证VC和VP的时候会使用，VC模板的格式参考[个人实名认证VC模板](#section5)。

```go
// SetVcTemplate 设置vc模板
// @param id string 模板ID
// @param name string 模板名称
// @param shortName string 模板短名（可选）
// @param vcType string 模板分类类型
// @param version string 模板版本
// @param template string vc模板内容,JSON schema格式
// @param creatorDid string 模板创建人的DID（可选）
// @return error 设置失败则返回Status：ERROR，Message具体错误
SetVcTemplate(id string, name string, shortName string, vcType string, version string, template string, creatorDid string) error
```

##### GetVcTemplate

```go
// GetVcTemplate 获取vc模板
// @param id string 要获取的模板ID
// @return *VcTemplate 返回查询到的vc模板
// @return error 查询失败则返回Status：ERROR，Message具体错误
GetVcTemplate(id string,version string) (*VcTemplate, error)
```

##### GetVcTemplateList

```go
// GetVcTemplateList 获取vc模板列表
// @param nameSearch string 搜索的模板名称关键字
// @param start int 开始的索引
// @param count int 要获取的数量
// @return []*VcTemplate 返回查询到的vc模板列表
// @return error 查询失败则返回Status：ERROR，Message具体错误
GetVcTemplateList(nameSearch string, start int, count int) ([]*VcTemplate, error)
```

##### EmitSetVcTemplateEvent

```go
// EmitSetVcTemplateEvent 发送设置vc模板事件
// @param templateID string 被设置的模板ID
// @param templateName string 被设置的模板名称
// @param version string 被设置的模板版本
// @param vcTemplate string 被设置的vc模板内容
EmitSetVcTemplateEvent(templateID string, templateName string, vcType string, version string, vcTemplate string, shortName string, creatorDid string)
```

##### DisableVcTemplate

```go
// DisableVcTemplate 禁用vc模板
DisableVcTemplate(templateID, version string) error
```

#### EnableVcTemplate

```go
//EnableVcTemplate 启用vc模板
EnableVcTemplate(templateID, version string) error
```

#### GetDisabledVcTemplateList

```go
// GetDisabledVcTemplateList 获取禁用vc模板列表
GetDisabledVcTemplateList(start int, count int) ([]*VcTemplate, error)
```

#### EmitDisableVcTemplateEvent

```go
// EmitDisableVcTemplateEvent 发送禁用vc模板事件
EmitDisableVcTemplateEvent(templateID, version string)
```

#### EmitEnableVcTemplateEvent

```go
// EmitEnableVcTemplateEvent 发送启用vc模板事件
EmitEnableVcTemplateEvent(templateID, version string)
```

##### VcIssueLog

```go
// VcIssueLog 记录vc发行日志
// @param issuer 必填，发行者DID
// @param did 必填，vc持有者DID
// @param templateID 选填，vc模板ID
// @param templateVersion 选填，vc模板对应版本
// @param vcID 必填，vcID或者vc hash
VcIssueLog(issuer string, did string, templateID string, templateVersion string, vcID string) error
```

##### GetVcIssueLogs

```go
// GetVcIssueLogs 获取vc发行日志
// @param issuer string 发行者DID
// @param did string vc持有者DID
// @param templateID string vc模板ID
// @param templateVersion 选填，vc模板对应版本
// @param start int 开始的索引
// @param count int 要获取的数量
// @return []*VcIssueLog 返回查询到的vc发行日志
// @return error 查询失败则返回Status：ERROR，Message具体错误
GetVcIssueLogs(issuer string, did string, templateID string, templateVersion string, start int, count int) ([]*VcIssueLog, error)
```

##### GetVcIssuers

```go
// GetVcIssuers 根据持证人DID获取vc发行者DID列表
GetVcIssuers(did string) (issuerDid []string, err error)
```

##### EmitVcIssueLogEvent

```go
// EmitVcIssueLogEvent 发送vc发行日志事件
// @param issuer string 发行者DID
// @param did string vc持有者DID
// @param templateID string vc模板ID
// @param templateVersion 选填，vc模板对应版本
// @param vcID string vcID或者vc hash
EmitVcIssueLogEvent(issuer string, did string, templateID, templateVersion string, vcID string)
```

以上是可拓展方法中每个函数的详细介绍。

## 事件

### Topic: SetDidDocument

data:

* did string
* didDocument string

### Topic: SetTrustRootList

data:

* dids []string

### Topic: RevokeVc

data:

* vcID1 string, vcID2 string, vcID3 string ...

### Topic: AddBlackList

data:

* dids []string

### Topic: DeleteBlackList

data:

* dids []string

### Topic: AddTrustIssuer

data:

* dids []string

### Topic: DeleteTrustIssuer

data:

* dids []string

### Topic: Delegate

data:

* delegatorDid string
* delegateeDid string
* resource string
* action string
* expiration *time.Time

### Topic: RevokeDelegate

data:

* delegatorDid string
* delegateeDid string
* resource string
* action string

### Topic: SetVcTemplate

data:

* templateID string
* templateName string
* vcType string
* version string
* vcTemplate string
* shortName string(如果有)
* creatorDid string(如果有)

### Topic: VcIssueLog

data:

* issuer string
* did string
* templateID string
* vcID string
* templateVersion string

### Topic: DisableVcTemplate

data:

* id string
* version string

### Topic: EnableVcTemplate

data:

* id string
* version string

## 参考

[DID标准协议](https://www.w3.org/TR/did-core/)

## 字段说明

### **DID文档（Document）**


| 字段名             | 备注                              |
| :----------------- | --------------------------------- |
| @context           | 上下文，Json LD                   |
| id                 | 使用uuid32位字符串生成            |
| created            | 创建时间                          |
| updated            | 更新时间                          |
| verificationMethod | 包含公钥信息，用于验签            |
| authentication     | 身份验证，公钥ID                  |
| controller         | 可以对DID document进行修改的实体  |
| unionId            | 引用外部DID                       |
| service            | 服务（IssuerDID文档中特有的字段） |
| proof              | 用于证明 DID 文档的数字签名信息   |

### **凭证（VerifiableCredential）**


| 字段名            | 备注                     |
| ----------------- | ------------------------ |
| id                | VC的ID                   |
| type              | VC类型                   |
| issuer            | 发行者DID                |
| issuanceDate      | 颁发日期                 |
| expirationDate    | 过期时间                 |
| credentialSubject | 主题（证书主体）信息     |
| template          | 模板信息                 |
| proof             | 用于证明VC的数字签名信息 |

### **可验证凭证（VerifiablePresentation）**


| 字段名               | 备注                     |
| :------------------- | ------------------------ |
| id                   | VP的ID                   |
| type                 | VP类型                   |
| verifiableCredential | VC列表                   |
| presentationUsage    | 使用场景                 |
| expirationDate       | VP过期时间               |
| verifier             | VP的验证方DID            |
| proof                | 用于证明VP的数字签名信息 |

### **公钥结构（VerificationMethod）**


| 字段名       | 备注                       |
| :----------- | -------------------------- |
| id           | 公钥ID                     |
| type         | 公钥类型                   |
| controller   | 拥有者的DID                |
| publicKeyPem | 公钥PEM                    |
| address      | 链账户地址，使用以太坊标准 |

### **证明结构（Proof）**


| 字段名             | 备注                           |
| :----------------- | ------------------------------ |
| type               | 签名算法类型                   |
| created            | 签名创建时间                   |
| proofPurpose       | 签名目的                       |
| verificationMethod | 公钥ID                         |
| proofValue         | 签名值                         |
| challenge          | 防止重放攻击（VP中特有的字段） |
| signedFields       | 签名字段                       |
| excludedFields     | 排除字段                       |

### **DID服务（Service）**


| 字段名          | 备注     |
| :-------------- | -------- |
| id              | 服务ID   |
| type            | 服务类型 |
| serviceEndpoint | 服务地址 |

### **VC模版（VcTemplate）**


| 字段名  | 备注     |
| ------- | -------- |
| id      | 模版ID   |
| name    | 模版名称 |
| version | 模版版本 |

## 示例数据

### 单公私钥对应的DID文档

<a id="section1"></a>
用户使用自己的公钥放入verificationMethod的publicKeyPem，使用私钥对除了proof外的整个Json字符串进行签名，并将签名结果放入proof的proofValue中。

```json
{
  "@context": "https://www.w3.org/ns/did/v1",
  "id": "did:cmid:123456789abcdefghi",
  "created": "2022-01-01T00:00:00Z",
  "updated": "2022-01-10T10:00:00Z",
  "verificationMethod": [
    {
      "id": "did:cmid:123456789abcdefghi#keys-1",
      "type": "SM2VerificationKey2022",
      "controller": "did:cmid:123456789abcdefghi",
      "publicKeyPem": "-----BEGIN PUBLIC KEY-----\nMFkwEwYHKoZIzj0CAQYIKoEcz1UBgi0DQgAEYbBKJ5xqkUaxYOoJlKkZIb2rhoVw\nZbjmyF9BRmOiBdp5Jde3QswKjicjMccB299I2n5UgQKdU8nPAY69Qiv5/w==\n-----END PUBLIC KEY-----",
      "address": "0x2B5AD5c4795c026514f8317c7a215E218DcCD6cF"
    }
  ],
  "authentication": [
    "did:cmid:123456789abcdefghi#keys-1"
  ],
  "controller": ["did:cmid:123456789abcdefghi", "did:cmid:admin"],
  "proof": [{
    "type": "SM2Signature",
    "created": "2022-01-01T00:00:00Z",
    "proofPurpose": "assertionMethod",
    "verificationMethod": "did:cmid:123456789abcdefghi#keys-1",
    "proofValue": "eyJhbGciOiJFUzI1NksiLCJraWQiOiJkaWQ6ZXhhbXBsZToxMjM0NTY3ODlhYmNkZWZnaGlfa2V5LTEiLCJ0eXAiOiJKV1MifQ..Q9JYDNOU0oyJkXW5NcC1hR3U4SHN6U1RiY3pvYkUzam5vY3VtY2tjZERxY3dLd1Z0a1d0Z2pUa0dWY3A0bFZJZw"
  }]
}

```

### 多公私钥对应的DID文档

<a id="section2"></a>
用户使用了多个公私钥对，将每个公钥放入verificationMethod的publicKeyPem，使用对应的私钥对除了proof外的整个Json字符串进行签名，并将签名结果放入proof的proofValue中。

```json
{
  "@context": "https://www.w3.org/ns/did/v1",
  "id": "did:cmid:123456789abcdefghi",
  "created": "2022-01-01T00:00:00Z",
  "updated": "2022-01-10T10:00:00Z",
  "verificationMethod": [
    {
      "id": "did:cmid:123456789abcdefghi#keys-1",
      "type": "SM2VerificationKey2022",
      "controller": "did:cmid:123456789abcdefghi",
      "publicKeyPem": "-----BEGIN PUBLIC KEY-----\nMFkwEwYHKoZIzj0CAQYIKoEcz1UBgi0DQgAEYbBKJ5xqkUaxYOoJlKkZIb2rhoVw\nZbjmyF9BRmOiBdp5Jde3QswKjicjMccB299I2n5UgQKdU8nPAY69Qiv5/w==\n-----END PUBLIC KEY-----",
      "address": "0x2B5AD5c4795c026514f8317c7a215E218DcCD6cF"
    },
    {
      "id": "did:cmid:123456789abcdefghi#keys-2",
      "type": "SM2VerificationKey2022",
      "controller": "did:cmid:123456789abcdefghi",
      "publicKeyPem": "-----BEGIN PUBLIC KEY-----\nMFkwEwYHKoZIzj0CAQYIKoEcz1UBgi0DQgAEYbBKJ5xqkUaxYOoJlKkZIb2rhoVw\nZbjmyF9BRmOiBdp5Jde3QswKjicjMccB299I2n5UgQKdU8nPAY69Qiv5/w==\n-----END PUBLIC KEY-----",
      "address": "0x3B5AD5c4795c026514f8317c7a215E218DcCD6cF"
    }
  ],
  "authentication": [
    "did:cmid:123456789abcdefghi#keys-1",
    "did:cmid:123456789abcdefghi#keys-2"
  ],
  "controller": ["did:cmid:123456789abcdefghi", "did:cmid:admin"],
  "proof": [{
    "type": "SM2Signature",
    "created": "2022-01-01T00:00:00Z",
    "proofPurpose": "assertionMethod",
    "signedFields": ["id"],
    "verificationMethod": "did:cmid:123456789abcdefghi#keys-1",
    "proofValue": "eyJhbGciOiJFUzI1NksiLCJraWQiOiJkaWQ6ZXhhbXBsZToxMjM0NTY3ODlhYmNkZWZnaGlfa2V5LTEiLCJ0eXAiOiJKV1MifQ..Q9JYDNOU0oyJkXW5NcC1hR3U4SHN6U1RiY3pvYkUzam5vY3VtY2tjZERxY3dLd1Z0a1d0Z2pUa0dWY3A0bFZJZw"
    },
    {
      "type": "SM2Signature",
      "created": "2022-01-01T00:00:00Z",
      "proofPurpose": "assertionMethod",
      "signedFields": ["id"],
      "verificationMethod": "did:cmid:123456789abcdefghi#keys-2",
      "proofValue": "eyJhbGciOiJFUzI1NksiLCJraWQiOiJkaWQ6ZXhhbXBsZToxMjM0NTY3ODlhYmNkZWZnaGlfa2V5LTEiLCJ0eXAiOiJKV1MifQ..Q9JYDNOU0oyJkXW5NcC1hR3U4SHN6U1RiY3pvYkUzam5vY3VtY2tjZERxY3dLd1Z0a1d0Z2pUa0dWY3A0bFZJZw"
    }
  ]
}
```

### Issuer的DID文档

<a id="section21"></a>

```json
{
  "@context": "https://www.w3.org/ns/did/v1",
  "id": "did:cmid:issuer1",
  "created": "2022-01-01T00:00:00Z",
  "updated": "2022-01-10T10:00:00Z",
  "verificationMethod": [
    {
      "id": "did:cmid:issuer1#keys-1",
      "type": "SM2VerificationKey2022",
      "controller": "did:cmid:issuer1",
      "publicKeyPem": "-----BEGIN PUBLIC KEY-----\nMFkwEwYHKoZIzj0CAQYIKoEcz1UBgi0DQgAEYbBKJ5xqkUaxYOoJlKkZIb2rhoVw\nZbjmyF9BRmOiBdp5Jde3QswKjicjMccB299I2n5UgQKdU8nPAY69Qiv5/w==\n-----END PUBLIC KEY-----",
      "address": "0x2B5AD5c4795c026514f8317c7a215E218DcCD6cF"
    }
  ],
  "authentication": [
    "did:cmid:issuer1#keys-1"
  ],
  "controller": ["did:cmid:issuer1", "did:cmid:admin"],
  "service": [
    {
      "id": "did:cmid:issuer1#issuerService",
      "type": "IssuerService",
      "serviceEndpoint": "https://issuer.example.com/service"
    }
  ],
  "proof": [{
    "type": "SM2Signature",
    "created": "2022-01-01T00:00:00Z",
    "proofPurpose": "assertionMethod",
    "verificationMethod": "did:cmid:issuer1#keys-1",
    "proofValue": "eyJhbGciOiJFUzI1NksiLCJraWQiOiJkaWQ6ZXhhbXBsZToxMjM0NTY3ODlhYmNkZWZnaGlfa2V5LTEiLCJ0eXAiOiJKV1MifQ..Q9JYDNOU0oyJkXW5NcC1hR3U4SHN6U1RiY3pvYkUzam5vY3VtY2tjZERxY3dLd1Z0a1d0Z2pUa0dWY3A0bFZJZw"
  }]
}

```

### 使用unionId描述外部DID，并且使用Proof中的excludedFields排除unionId字段

<a id="section1"></a >
用户使用自己的公钥放入verificationMethod的publicKeyPem，使用私钥对除了proof外的整个Json字符串进行签名，并将签名结果放入proof的proofValue中。

```json
{
  "@context": "https://www.w3.org/ns/did/v1",
  "id": "did:cmid:123456789abcdefghi",
  "created": "2022-01-01T00:00:00Z",
  "updated": "2022-01-10T10:00:00Z",
  "unionId": {
    "ctid": "did:ctid:123456789abcdefghi",
    "xxxx": "did:ctid:xxxx:123456789abcdefghi"
  },
  "verificationMethod": [{
    "id": "did:cmid:123456789abcdefghi#keys-1",
    "type": "SM2VerificationKey2022",
    "controller": "did:cmid:123456789abcdefghi",
    "publicKeyPem": "-----BEGIN PUBLIC KEY-----\nMFkwEwYHKoZIzj0CAQYIKoEcz1UBgi0DQgAEYbBKJ5xqkUaxYOoJlKkZIb2rhoVw\nZbjmyF9BRmOiBdp5Jde3QswKjicjMccB299I2n5UgQKdU8nPAY69Qiv5/w==\n-----END PUBLIC KEY-----",
    "address": "0x2B5AD5c4795c026514f8317c7a215E218DcCD6cF"
  },
    {
      "id": "did:cmid:123456789abcdefghi#keys-2",
      "type": "SM2VerificationKey2022",
      "controller": "did:cmid:123456789abcdefghi",
      "publicKeyPem": "-----BEGIN PUBLIC KEY-----\nMFkwEwYHKoZIzj0CAQYIKoEcz1UBgi0DQgAEYbBKJ5xqkUaxYOoJlKkZIb2rhoVw\nZbjmyF9BRmOiBdp5Jde3QswKjicjMccB299I2n5UgQKdU8nPAY69Qiv5/w==\n-----END PUBLIC KEY-----",
      "address": "0x3B5AD5c4795c026514f8317c7a215E218DcCD6cF"
    }
  ],
  "authentication": [
    "did:cmid:123456789abcdefghi#keys-1",
    "did:cmid:123456789abcdefghi#keys-2"
  ],
  "controller": ["did:cmid:123456789abcdefghi", "did:cmid:admin"],
  "proof": [{
    "type": "SM2Signature",
    "created": "2022-01-01T00:00:00Z",
    "proofPurpose": "assertionMethod",
    "verificationMethod": "did:cmid:123456789abcdefghi#keys-1",
    "proofValue": "eyJhbGciOiJFUzI1NksiLCJraWQiOiJkaWQ6ZXhhbXBsZToxMjM0NTY3ODlhYmNkZWZnaGlfa2V5LTEiLCJ0eXAiOiJKV1MifQ..Q9JYDNOU0oyJkXW5NcC1hR3U4SHN6U1RiY3pvYkUzam5vY3VtY2tjZERxY3dLd1Z0a1d0Z2pUa0dWY3A0bFZJZw",
    "excludedFields": [
      "unionId"
    ]
  },
    {
      "type": "SM2Signature",
      "created": "2022-01-01T00:00:00Z",
      "proofPurpose": "assertionMethod",
      "verificationMethod": "did:cmid:123456789abcdefghi#keys-2",
      "proofValue": "eyJhbGciOiJFUzI1NksiLCJraWQiOiJkaWQ6ZXhhbXBsZToxMjM0NTY3ODlhYmNkZWZnaGlfa2V5LTEiLCJ0eXAiOiJKV1MifQ..Q9JYDNOU0oyJkXW5NcC1hR3U4SHN6U1RiY3pvYkUzam5vY3VtY2tjZERxY3dLd1Z0a1d0Z2pUa0dWY3A0bFZJZw"
    }
  ]
}
```

### 

### 个人实名认证VC

<a id="section3"></a>

```json
{
  "@context": [
    "https://www.w3.org/2018/credentials/v1",
    "https://www.w3.org/2018/credentials/examples/v1"
  ],
  "id": "https://example.com/credentials/123",
  "type": ["VerifiableCredential", "IdentityCredential"],
  "issuer": "did:cmid:gongan1234",
  "issuanceDate": "2022-01-01T00:00:00Z",
  "expirationDate": "2042-01-01T00:00:00Z",
  "credentialSubject": {
    "id": "did:cmid:123456789abcdefghi",
    "certificateName": "长安链实名认证证书",
    "name": "张三",
    "idNumber": "123456789012345678",
    "phoneNumber": "13800138000"
  },
  "template": {
    "id": "456",
    "name": "个人身份认证",
    "version": "1.0"
  },
  "proof": [{
    "type": "SM2Signature",
    "created": "2022-01-01T00:00:00Z",
    "proofPurpose": "assertionMethod",
    "verificationMethod": "did:cmid:gongan1234#keys-1",
    "proofValue": "eyJhbGciOiJFUzI1NksiLCJraWQiOiJkaWQ6Y21pZDpnYW5nYW4xMjM0X2tleS0xIiwidHlwIjoiSldTIn0..Q9JYDNOU0oyJkXW5NcC1hR3U4SHN6U1RiY3pvYkUzam5vY3VtY2tjZERxY3dLd1Z0a1d0Z2pUa0dWY3A0bFZJZw"
  }]
}
```

### 个人实名亮证

<a id="section4"></a>

```json
{
  "@context": [
    "https://www.w3.org/2018/credentials/v1",
    "https://www.w3.org/2018/credentials/examples/v1"
  ],
  "type": "VerifiablePresentation",
  "id": "https://example.com/presentations/123",
  "verifiableCredential": [
    {
      "id": "https://example.com/credentials/123",
      "type": ["VerifiableCredential", "IdentityCredential"],
      "issuer": "did:cmid:gongan1234",
      "issuanceDate": "2022-01-01T00:00:00Z",
      "expirationDate": "2023-01-01T00:00:00Z",
      "holder": "did:cmid:123456789abcdefghi",
      "credentialSubject": {
        "id": "did:cmid:123456789abcdefghi",
        "name": "张三",
        "idNumber": "123456789012345678",
        "phoneNumber": "13800138000"
      },
      "template": {
        "id": "456",
        "name": "个人身份认证",
        "version": "1.0"
      },
      "proof": [{
        "type": "SM2Signature",
        "created": "2022-01-01T00:00:00Z",
        "proofPurpose": "assertionMethod",
        "verificationMethod": "did:cmid:gongan1234#keys-1",
        "proofValue": "eyJhbGciOiJFUzI1NksiLCJraWQiOiJkaWQ6Y21pZDpnYW5nYW4xMjM0X2tleS0xIiwidHlwIjoiSldTIn0..Q9JYDNOU0oyJkXW5NcC1hR3U4SHN6U1RiY3pvYkUzam5vY3VtY2tjZERxY3dLd1Z0a1d0Z2pUa0dWY3A0bFZJZw"
      }]
    }
  ],
  "presentationUsage": "身份验证",
  "expirationDate": "2024-01-01T00:00:00Z",
  "verifier": "did:cmid:verifier1234",
  "proof": [{
    "type": "SM2Signature",
    "created": "2022-01-01T00:00:00Z",
    "proofPurpose": "authentication",
    "challenge": "5f56a1cb-8d8e-4d8b-abe3-47a9d9db3f48",
    "verificationMethod": "did:cmid:123456789abcdefghi#keys-1",
    "proofValue": "eyJhbGciOiJFUzI1NksiLCJraWQiOiJkaWQ6ZXhhbXBsZToxMjM0NTY3ODlhYmNkZWZnaGlfa2V5LTEiLCJ0eXAiOiJKV1MifQ..Q9JYDNOU0oyJkXW5NcC1hR3U4SHN6U1RiY3pvYkUzam5vY3VtY2tjZERxY3dLd1Z0a1d0Z2pUa0dWY3A0bFZJZw"
  }]
}
```

### VC模板

<a id="section5"></a>
VC模板是一个JSON schema，用于验证VC的内容的credentialSubject是否符合模板要求。

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "type": "object",
  "properties": {
    "name": {
      "type": "string",
      "title": "姓名"
    },
    "idNumber": {
      "type": "string",
      "title": "身份证号"
    },
    "phoneNumber": {
      "type": "string",
      "title": "手机号"
    }
  },
  "required": ["name", "idNumber", "phoneNumber"],
  "additionalProperties": true
}

```

#### VC 无隐私签名

```json
{
  "@context": [
    "https://www.w3.org/2018/credentials/v1",
    "https://www.w3.org/2018/credentials/examples/v1"
  ],
  "id": "https://example.com/credentials/123",
  "type": ["VerifiableCredential", "IdentityCredential"],
  "issuer": "did:cmid:gongan1234",
  "issuanceDate": "2022-01-01T00:00:00Z",
  "expirationDate": "2042-01-01T00:00:00Z",
  "holder": "did:cmid:123456789abcdefghi",
  "credentialSubject": {
    "id": "did:cmid:123456789abcdefghi",
    "certificateName": "长安链实名认证证书",
    "name": "张三",
    "idNumber": "123456789012345678",
    "phoneNumber": "13800138000"
  },
  "template": {
    "id": "456",
    "name": "个人身份认证",
    "version": "1.0"
  },
  "proof": [
  {
    "type": "SM2Signature",
    "created": "2022-01-01T00:00:00Z",
    "proofPurpose": "assertionMethod",
    "verificationMethod": "did:cmid:gongan1234#keys-1",
    "proofValue": "eyJhbGciOiJFUzI1NksiLCJraWQiOiJkaWQ6Y21pZDpnYW5nYW4xMjM0X2tleS0xIiwidHlwIjoiSldTIn0..Q9JYDNOU0oyJkXW5NcC1hR3U4SHN6U1RiY3pvYkUzam5vY3VtY2tjZERxY3dLd1Z0a1d0Z2pUa0dWY3A0bFZJZw"
  },
  {
    "type": "SM2Signature",
    "created": "2022-01-01T00:00:00Z",
    "proofPurpose": "assertionMethod",
    "verificationMethod": "did:cmid:gongan1234#keys-1",
    "excludedFields": ["credentialSubject"],
    "proofValue": "eyJhbGciOiJFUzI1NksiLCJraWQiOiJkaWQ6Y21pZDpnYW5nYW4xMjM0X2tleS0xIiwidHlwIjoiSldTIn0..Q9JYDNOU0oyJkXW5NcC1hR3U4SHN6U1RiY3pvYkUzam5vY3VtY2tjZERxY3dLd1Z0a1d0Z2pUa0dWY3A0bFZJZw"
  }
  ]
}
```

#### VP无隐私的亮证

```json
{
  "@context": [
    "https://www.w3.org/2018/credentials/v1",
    "https://www.w3.org/2018/credentials/examples/v1"
  ],
  "type": "VerifiablePresentation",
  "id": "https://example.com/presentations/123",
  "verifiableCredential": [
    {
      "id": "https://example.com/credentials/123",
      "type": ["VerifiableCredential", "IdentityCredential"],
      "issuer": "did:cmid:gongan1234",
      "issuanceDate": "2022-01-01T00:00:00Z",
      "expirationDate": "2023-01-01T00:00:00Z",
      "holder": "did:cmid:123456789abcdefghi",
   
      "template": {
        "id": "456",
        "name": "个人身份认证",
        "version": "1.0"
      },
      "proof": [{
        "type": "SM2Signature",
        "created": "2022-01-01T00:00:00Z",
        "proofPurpose": "assertionMethod",
        "excludedFields": ["credentialSubject"],
        "verificationMethod": "did:cmid:gongan1234#keys-1",
        "proofValue": "eyJhbGciOiJFUzI1NksiLCJraWQiOiJkaWQ6Y21pZDpnYW5nYW4xMjM0X2tleS0xIiwidHlwIjoiSldTIn0..Q9JYDNOU0oyJkXW5NcC1hR3U4SHN6U1RiY3pvYkUzam5vY3VtY2tjZERxY3dLd1Z0a1d0Z2pUa0dWY3A0bFZJZw"
      }]
    }
  ],
  "presentationUsage": "身份验证",
  "expirationDate": "2024-01-01T00:00:00Z",
  "verifier": "did:cmid:verifier1234",
  "proof": [{
    "type": "SM2Signature",
    "created": "2022-01-01T00:00:00Z",
    "proofPurpose": "authentication",
    "challenge": "5f56a1cb-8d8e-4d8b-abe3-47a9d9db3f48",
    "verificationMethod": "did:cmid:123456789abcdefghi#keys-1",
    "proofValue": "eyJhbGciOiJFUzI1NksiLCJraWQiOiJkaWQ6ZXhhbXBsZToxMjM0NTY3ODlhYmNkZWZnaGlfa2V5LTEiLCJ0eXAiOiJKV1MifQ..Q9JYDNOU0oyJkXW5NcC1hR3U4SHN6U1RiY3pvYkUzam5vY3VtY2tjZERxY3dLd1Z0a1d0Z2pUa0dWY3A0bFZJZw"
  }]
}
```

## 示例合约

[CMDID-go](https://git.chainweaver.org.cn/chainweaver/did/did-contract)
