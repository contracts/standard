# CM-CS-221221-NFA（CMNFA-1）数字藏品类合约标准协议整体介绍

## 背景及目的
长安链非同质化数字资产合约标准`CM-CS-221221-NFA`全称`Chainmaker - Contract Standard - Digital Non-Fungible Assets-20221221版`，简称`CMNFA-1`，
是官方推出的基于长安链的数字藏品类合约的撰写和使用规范。
方便大家在编写相关合约时能够做到有章可循，长安链的各生态工具也会基于此标准对相应的合约进行适配。

## 适用场景
本标准是长安链Go语言合约的NFA标准，适用于各类数字藏品业务的场景。和以太坊NFA协议不同的是，本标准不再区分ERC721
和ERC1155，而是在参考两者功能的情况下，提供了更多扩展功能。

# 协议详情

## 基础方法介绍

### 合约函数集
| 分类      | 函数名               | 函数类型 | 函数说明                           | 备注  |
|---------|-------------------|------|--------------------------------|-----|
| 发行      | Mint              | 调用   | 增发指定的NFA至某个账号                  | 必须  | 
| 发行      | MintBatch         | 调用   | 批量发行NFA                        | 必须  |
| 授权      | SetApproval       | 调用   | 授权某个账户转账自己账户下的NFA              | 必须  |
| 授权      | SetApprovalForAll | 调用   | 授权某个账户转账自己账户下的所有NFA            | 必须  |
| 转账      | TransferFrom      | 调用   | 从指定账户转账NFA                     | 必须  |
| 转账      | TransferFromBatch | 调用   | 从指定账户匹量转账NFA                   | 必须  |
| 查询NFA   | OwnerOf           | 查询   | 查询指定NFA的所属账号                   | 必须  | 
| 查询NFA   | TokenURI          | 查询   | 查询NFA的URI(categoryURI+tokenId) | 必须  |


## 合约方法介绍

```go
// Category, the tokens' category info
type Category struct {
    // CategoryName, the name of the category
    CategoryName string `json:"categoryName"`
    // CategoryURI, the uri of the category
    CategoryURI string  `json:"categoryURI"`
}

// NFA, a Non-Fungible Assets token
type NFA struct {
	// TokenId, the id of the token
    TokenId      string `json:"tokenId"`
	// CategoryName, the name of the category
    CategoryName string `json:"categoryName"`
	// To, the address which the token minted to
    To           string `json:"to"`
	// Metadata, the metadata of the token
    Metadata     []byte `json:"metadata"`
}

// AccountTokens
type AccountTokens struct {
	// Account
    Account string
	// Tokens, id list of "Account"
    Tokens  []string
}
```

### Mint
```go
// Mint a token, Obligatory.
// @param to, the owner address of the token. Obligatory.
// @param tokenId, the id of the token. Obligatory.
// @param categoryName, the name of the category. If categoryName is empty, the token's category
// will be the default category. Optional.
// @param metadata, the metadata of the token. Optional.
// @return error, the error msg if some error occur.
// @event, topic: 'mint'; data: to, tokenId, categoryName, metadata
Mint(to, tokenId, categoryName string, metadata []byte) error
```
- 官方建议，所发行的NFA-matedata信息至少需要包含如下字段，作品名称、作者名、发行机构、作品URL、作品描述、作品哈希。例：

```json
 {
      "author":"张三出品",
      "orgName":"北京美好景象图片有限公司",
      "name":"Lionel Messi",
      "description":"利昂内尔·安德烈斯·“利奥”·梅西·库奇蒂尼，简称梅西（西班牙语：Lionel Messi），生于阿根廷圣菲省罗萨里奥，现正效力于法甲俱乐部巴黎圣日耳曼，同时担任阿根廷国家足球队队长，司职边锋、前锋及前腰。目前他共获得7座金球奖、6次世界足球先生及6座欧洲金靴奖。",
      "image":"https://www.strikers.auction/images/cards/000.png",
      "seriesHash":"5fabfb28760f946a233b58e99bfac43f3c53b19afa41d26ea75a3a58cbfc1491"
    }
```

- 请注意matedata必须是一段标准的json，若希望长安链官方生态工具如Web3插件，区块链浏览器等能解析出相关数据，则字段名称需与上述例子保持一致。其中
  - 作品URL为该NFA图片资源存放的地址，
  - 作品哈希为该图片对应的sha256哈希值，通过将资源哈希值上链进行存证，确保就算是存储在中心化云服务的NFA也不可被篡改。


### MintBatch
```go
// MintBatch mint nfa tokens batch. Obligatory.
// @param tokens, the tokens to mint. Obligatory.
// @return error, the error msg if some error occur.
// @event, topic: 'mintBatch'; data: tokens
MintBatch(tokens []NFA) error
```

### SetApproval
```go
// SetApproval approve or cancel approve token to 'to' account. Obligatory.
// @param to, destination approve to. Obligatory.
// @param tokenId, the token id. Obligatory.
// @param isApproval, to approve or to cancel approve
// @return error, the error msg if some error occur.
// @event, topic: 'SetApproval'; data: to, tokenId, isApproval
SetApproval(to, tokenId string, isApproval bool) error
```

### SetApprovalForAll
```go
// SetApprovalForAll approve or cancel approve all token to 'to' account. Obligatory.
// @param to, destination address approve to. Obligatory.
// @isApprove, true means approve and false means cancel approve. Obligatory.
// @return error, the error msg if some error occur.
// @event, topic: 'SetApprovalForAll'; data: to, isApproval
SetApprovalForAll(to, isApproval bool) error
```

### TransferFrom
```go
// TransferFrom transfer single token after approve. Obligatory.
// @param from, owner account of token. Obligatory.
// @param to, destination account transferred to. Obligatory.
// @param tokenId, the token being transferred. Obligatory.
// @return error, the error msg if some error occur.
// @event, topic: 'TransferFrom'; data: from, to, tokenId
TransferFrom(from, to, tokenId string) error
```

### TransferFromBatch
```go
// TransferFromBatch transfer tokens after approve. Obligatory.
// @param from, owner account of token. Obligatory.
// @param to, destination account transferred to. Obligatory.
// @param tokenIds, the tokens being transferred. Obligatory.
// @return error, the error msg if some error occur.
// @event, topic: 'TransferFromBatch'; data: from, to, tokenIds
TransferFromBatch(from, to, tokenIds []string) error
```

### OwnerOf
```go
// OwnerOf get the owner of a token. Obligatory.
// @param tokenId, the token which will be queried. Obligatory.
// @return account, the token's account.
// @return err, the error msg if some error occur.
OwnerOf(tokenId string) (account string, err error)
```

### TokenURI
```go
// TokenURI get the URI of the token. a token's uri consists of CategoryURI and tokenId. Obligatory.
// @param tokenId, tokenId be queried. Obligatory.
// @return uri, the uri of the token.
// @return err, the error msg if some error occur.
TokenURI(tokenId string) (uri string, err error)

```

## 可拓展方法介绍

### 合约函数集
| 分类    | 函数名                   | 函数类型 | 函数说明                   | 备注  |
|-------|-----------------------|------|------------------------|-----|
| 授权    | SetApprovalByCategory | 调用   | 授权某个账户转账自己的某个分类下的所有NFA | 可选  |
| 分类管理  | CreateOrSetCategory   | 调用   | 创建分类                   | 可选  |
| 销毁    | Burn                  | 调用   | 销毁token                | 可选 |
| 分类管理  | GetCategoryByName     | 查询   | 根据名称查询分类信息             | 可选  |
| 分类管理  | GetCategoryByTokenId  | 查询   | 查找某个token的分类           | 可选  |
| 查询NFA | TotalSupply           | 查询   | 查找合约下总共发行的NFA总量        | 可选  |
| 查询NFA | TotalSupplyOfCategory | 查询   | 查询某个分类下发行的NFA总量        | 可选  |
| 查询NFA | BalanceOf             | 查询   | 查询某个账户下的NFA数量          | 可选  |
| 查询NFA | AccountTokens         | 查询   | 查询某个账户下的所有NFA          | 可选  |
| 查询NFA | TokenMetadata         | 查询   | 查询NFA的metadata         | 可选  |


### SetApprovalByCategory
```go
// SetApprovalByCategory approve or cancel approve tokens of category to 'to' account. Optional.
// @param to, destination address approve to. Obligatory.
// @categoryName, the category of tokens. Obligatory.
// @isApproval, to approve or to cancel approve. Obligatory.
// @return error, the error msg if some error occur.
// @event, topic: 'SetApprovalByCategory'; data: to, categoryName, isApproval
SetApprovalByCategory(to, categoryName string, isApproval bool) error
```

### CreateOrSetCategory
```go
// CreateOrSetCategory create ore reset a category. Optional.
// @param categoryName, the category name. Obligatory.
// @param categoryURI, the category uri. Obligatory.
// @return error, the error msg if some error occur.
// @event, topic: 'CreateOrSetCategory'; data: category
CreateOrSetCategory(category *Category) error
```

### Burn
```go
// Burn burn a token
// @param tokenId
// @event, topic: 'Burn'; data: tokenId
Burn(tokenId string) error
```

### GetCategoryByName
```go
// GetCategoryByName get specific category by name. Optional.
// @param categoryName, the name of the category. Obligatory.
// @return category, the category returned.
// @return err, the error msg if some error occur.
GetCategoryByName(categoryName string) (category *Category, err error)
```

### GetCategoryByTokenId
```go
// GetCategoryByTokenId get a specific category by tokenId. Optional.
// @param tokenId, the names of category to be queried. Obligatory.
// @return category, the result queried.
// @return err, the error msg if some error occur.
GetCategoryByTokenId(tokenId string) (category *Category, err error)
```

### TotalSupply
```go
// TotalSupply get total token supply of this contract. Obligatory.
// @return TotalSupply, the total token supply value returned.
// @return err, the error msg if some error occur.
TotalSupply() (totalSupply uint256, err error)
```

### TotalSupplyOfCategory
```go
// TotalSupplyOfCategory get total token supply of the category. Obligatory.
// @param category, the category of tokens. Obligatory.
// @return TotalSupply, the total token supply value returned.
// @return err, the error msg if some error occur.
TotalSupplyOfCategory(category string) (totalSupply uint256, err error)
```

### BalanceOf
```go
// BalanceOf get total token number of the account. Optional
// @param account, the account which will be queried. Obligatory.
// @return balance, the token number of the account.
// @return err, the error msg if some error occur.
BalanceOf(account string) (balance uint256, err error)
```

### AccountTokens
```go
// AccountTokens get the token list of the account. Optional
// @param account, the account which will be queried. Obligatory.
// @return tokenId, the list of tokenId.
// @return err, the error msg if some error occur.
AccountTokens(account string) (tokenId []string, err error)
```

### TokenMetadata
```go
// TokenMetadata get the metadata of a token. Optional.
// @param tokenId, tokenId which will be queried.
// @return metadata, the metadata of the token.
// @return err, the error msg if some error occur.
TokenMetadata(tokenId string) (metadata []byte, err error)
```

## 事件
### Topic: Mint
data:
1. to string
2. tokenId string
3. categoryName string
4. metadata []byte

### Topic: MintBatch
data:
1. tokens []NFA

### Topic: SetApprove
data:
1. to string
2. tokenId string
3. isApprove bool

### Topic: SetApproveForAll
data:
1. to string
2. isApprove bool

### Topic: TransferFrom
data:
1. from string
2. to string
3. tokenId string

### Topic: TransferFromBatch
data:
1. from string
2. to string
3. tokenIds []string

### Topic: SetApproveByCategory
data:
1. to string
2. categoryName string
3. isApprove bool

### Topic: CreateOrSetCategory
data:
1. category Category


## 参考
[ERC721协议规范](https://github.com/ethereum/EIPs/blob/master/EIPS/eip-721.md)
[ERC1155协议规范](https://github.com/ethereum/EIPs/blob/master/EIPS/eip-1155.md)

## 示例合约
[CMNFA-go](https://git.chainmaker.org.cn/contracts/contracts-go/-/tree/master/standard-nfa)